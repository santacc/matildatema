{{--
  Template Name: Template Programa
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <?php
  $contBotones = '';
  $fondoMovil = get_field('fondoMovil');
  $imagenDesk = get_field('imagenDesk');
  if( have_rows('botonesLink') ):
    $contBotones .= '';
    while( have_rows('botonesLink') ) : the_row();
      $contBotones .='<div class="col-10 col-md-6 p-2">';
      $textBoton = get_sub_field('textBoton');
      $urlBoton = get_sub_field('urlBoton');

      $contBotones .= '<a href="'.$urlBoton.'" class="btn btn-primary btn-lg">'.$textBoton.'</a>';
        $contBotones .= '</div>';
    endwhile;

  else :

  endif;

  $contMovil = '<section style="background-image: url(https://musicalgrease.es/wp-content/uploads/2021/09/fondo_home-1.jpg)" id="seccionPrograma">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="d-none d-md-block col-md-5">
                                <img src='. $imagenDesk["url"] .' width="100%">
                            </div>
                            <div class="col-12 d-md-none col-md-5">
                                <div class="row paraMovil justify-content-center" style="background-image: url('. $fondoMovil["url"].' );">
                                  '. $contBotones .'
                                  <div class="col-10 col-md-6 p-2 text-center" style="align-items: center; align-content: center; align-self: center;">
	                                    <a href="https://www.facebook.com/GREASEelmusical" target="_blank" rel="noopener"><i class="fab fa-2x fa-facebook" style="margin-right: 10px; color: #ee5754"></i></a>
	                                    <a href="https://www.instagram.com/greasemusicaloficial/" target="_blank" rel="noopener"><i class="fab fa-2x fa-instagram" style="margin-right: 10px; color: #ee5754"></i></a>
	                                    <a href="https://twitter.com/_grease" target="_blank" rel="noopener"><i class="fab fa-2x fa-twitter" style="color: #ee5754"></i></a>
<a href="https://www.tiktok.com/@greasemusical" target="_blank" rel="noopener"><i class="fab fa-2x fa-tiktok" style="color: #ee5754; margin-left: 5px"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="d-none col-12 d-md-block col-md-5">
                                <div class="row paradesktop justify-content-center">
                                  '. $contBotones .'
                                  <div class="col-10 col-md-6 p-2 text-center" style="align-items: center; align-content: center; align-self: center;">
	                                          <a href="https://www.facebook.com/GREASEelmusical" target="_blank" rel="noopener"><i class="fab fa-2x fa-facebook" style="margin-right: 10px; color: #ee5754"></i></a>
	                                          <a href="https://www.instagram.com/greasemusicaloficial/" target="_blank" rel="noopener"><i class="fab fa-2x fa-instagram" style="margin-right: 10px; color: #ee5754"></i></a>
	                                          <a href="https://twitter.com/_grease" target="_blank" rel="noopener"><i class="fab fa-2x fa-twitter" style="color: #ee5754"></i></a>
<a href="https://www.tiktok.com/@greasemusical" target="_blank" rel="noopener"><i class="fab fa-2x fa-tiktok" style="color: #ee5754; margin-left: 5px"></i></a>
</div>
                                </div>
                            </div>
                    </section>';
  ?>


  @endwhile

  <?php
    echo $contMovil;

  ?>
@endsection



