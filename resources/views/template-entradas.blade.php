{{--
  Template Name: Template Entradas
--}}

<?php

if( have_rows('seccionSuperior') ){
  while( have_rows('seccionSuperior') ) {
    the_row();
    $seccionSuperior = get_sub_field('verSeccionSup');
  }
}
if( have_rows('informacionUtil') ){
  while( have_rows('informacionUtil') ){
    the_row();
    $informacionUtil = get_sub_field('verSeccionSup');
   }
}
if( have_rows('preciosTeatro') ){
  while( have_rows('preciosTeatro') ){
    the_row();
    $precioTeatro = get_sub_field('verSeccionSup');
   }
}
if( have_rows('tipoVenta') ){
  while( have_rows('tipoVenta') ){
    the_row();
    $tipoVenta = get_sub_field('verSeccionSup');
  }
}
if( have_rows('headerEspecial') ){
  while( have_rows('headerEspecial') ){
    the_row();
    $verHeaderEspecial = get_sub_field('verSeccionSup');
  }
}
if( have_rows('promocionEspecial') ){
  while( have_rows('promocionEspecial') ){
    the_row();
    $verSeccionPromocion = get_sub_field('verSeccionSup');
  }
}
if( have_rows('destacadosPromociones') ){
  while( have_rows('destacadosPromociones') ){
    the_row();
    $destacadosPromociones = get_sub_field('verSeccionSup');
  }
}
if( have_rows('tarjetaRegalo') ){
  while( have_rows('tarjetaRegalo') ){
    the_row();
    $verTarjetaRegalo = get_sub_field('verSeccionSup');
   }
}
if( have_rows('seccionAyuda') ){
  while( have_rows('seccionAyuda') ){
    the_row();
    $verSeccionAyuda = get_sub_field('verSeccionSup');
  }
};
?>
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="container">
      <div class="row">
        <div class="col-12" style="display: none">
          @include('partials.page-header')
        </div>
      </div>
    </div>
    @include('partials.content-page')

    <?php if($verHeaderEspecial == 1) { ?>
      @include('partials.entradas.headerEspecial')
    <?php } ?>
    <!-- Texto intro debajo del menú-->

    <?php if($seccionSuperior == 1) { ?>
        @include('partials.entradas.seccionSuperior')
    <?php } ?>
    <!-- Family pack -->
    <?php if($verSeccionPromocion == 1) { ?>
        @include('partials.entradas.promoDestacada')
    <?php } ?>
    <!-- Teléfono y taquillas-->
    <?php
    if($tipoVenta == 1) { ?>
      @include('partials.entradas.tipoVenta')
    <?php } ?>
    <!-- Butaca Oro /  Palco vip / Grupos -->
    <?php if($destacadosPromociones == 1) { ?>
      @include('partials.entradas.promociones')
    <?php } ?>
    <!-- Seccion de precios y plano teatro -->
    <?php if($precioTeatro==1) { ?>
      @include('partials.entradas.precios')
    <?php } ?>
    <!-- Informacion util-->
    <?php if($informacionUtil == 1) { ?>
      @include('partials.entradas.informacionUtil')
    <?php } ?>
    <!-- Tarjeta regalo -->
    <?php if($verTarjetaRegalo == 1) { ?>
      @include('partials.entradas.tarjetaRegalo')
  <?php } ?>

  @endwhile
@endsection
<!-- Te podemos ayudar -->


<script>
  jQuery("a.btnEntradas").attr("href", "https://tickets.musicalmatilda.es/janto/")
</script>
<!-- <div class="popupFamilias">
   <button type="button" class="btn-close btn-close-white botonCerrar" aria-label="Cerrar" id="cerrar"></button>

   <div class="container">
     <div class="row justify-content-center">
       <div class="col-12 col-md-4 p-4">

         <a href="https://tickets.musicalgrease.es/janto/" target="_blank"><img src="/wp-content/themes/temamusicales/dist/images/Family-Pack.png" width="100%"></a>


       </div>
       <div class="col-12 col-md-5 p-4" style="align-self: center">

         <p>Aprovecha nuestro pack 15% comprando 4 entradas o más en el área de platea en funciones seleccionadas.</p>
         <a href="https://tickets.musicalgrease.es/janto/" target="_blank" class="btn btn-blanco"  role="button" style="background-color: #ee5754; color: #fff">COMPRA TUS ENTRADAS</a>
       </div>
     </div>
   </div>
 </div> -->
