{{--
  Template Name: Template Opiniones
--}}

@extends('layouts.app')

@section('content')

  <?php


  $args = [
    'post_type' => 'opinion',
    'posts_per_page' => -1,


  ];

  $query = new WP_Query( $args );
  $contSlider = '';
  $i = 0;
  $bulletsSlide = '';
  ?>
  @while($query->have_posts()) @php $query->the_post() @endphp
  <?php
  $tituloOpinion = get_the_title();
  $descOpinion = get_field ('descOpinion');
  $nombreOpinion = get_field ('nombreOpinion');
  $fechaOpinion = get_field ('fechaOpinion');
  $txtMedioOpinion = get_field ('txtMedioOpinion');
  $nomMedioOpinion = get_field ('nomMedioOpinion');


  $contSlider .= '<li class="glide__slide" style="padding: 10px;">
                        <div class="contOpinion text-left">
                          <div class="titulOpinion">'.$tituloOpinion .' </div>
                          <div class="fechaOpininio">'. get_the_date('d/m/Y') .'</div>
                          <p class="mb-0 parrafOpinion"><i class="fas fa-quote-left"></i>'. $descOpinion .'<i class="fas fa-quote-right"></i></p>

                        </div>

                    </li>';


  $bulletsSlide .= '<button class="glide__bullet" data-glide-dir="='.$i.'"></button>';
  $i++;
  ?>

  @endwhile



  <section class="opinionesHomeDark">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="page-header">
            <h1>OPINIONES</h1>
          </div>
        </div>
      </div>
    </div>
      <div class="container py-5">
        <div class="row">
          <div class="col-12">

            <div class="glide " style="width: 88%; margin: 0 auto">
              <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">

                  <?php
                  echo $contSlider;
                  ?>
                </ul>
                <!-- <div class="glide__arrows" data-glide-el="controls">
                  <button class="glide__arrow glide__arrow--left" data-glide-dir="<"> < </button>
                  <button class="glide__arrow glide__arrow--right" data-glide-dir=">"> > </button>
                </div> -->
                <div class="glide__bullets" data-glide-el="controls[nav]">
                  <?php echo $bulletsSlide; ?>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>
  <section class="formOpiniones">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-8 py-4">
        <h3>Dejanos tu opinión</h3>
        <?php

        echo do_shortcode( '[gravityform id="3" title="false" description="false" ajax="true" tabindex="49" field_values="check=First Choice,Second Choice"]');

        ?>
      </div>
    </div>
  </div>
  </section>

@endsection



