{{--
  Template Name: Template Para teatro
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


  @include('partials.page-header')
  <?php

        $tituloDestacado = get_field('tituloDestacado');
        $titDesatcadoUno = get_field('titDesatcadoUno');
        $desDestacadoUno = get_field('desDestacadoUno');
        $botonMapa = get_field('botonMapa');
        $iframeMapa = get_field('iframeMapa');
        $horarioTeatro = get_field('horarioTeatro');
        $imagenTeatroSeguro = get_field('imagenTeatroSeguro');
        $linkTeatroSeguro = get_field('linkTeatroSeguro');
        $titComoLlegar = get_field('titComoLlegar');
        $titInfoTeatro = get_field('titInfoTeatro');
        $infoTeatro = get_field('infoTeatro');
        $contMediostransporte = '';
        $fondoSeccionUno = get_field('fondoSeccionUno');
        $colorTxtSeccionUno = get_field('colorTxtSeccionUno');
        $fondoSeccionDos = get_field('fondoSeccionDos');
        $colorTxtSeccionDos = get_field('colorTxtSeccionDos');
        $fondoSeccionTres = get_field('fondoSeccionTres');
        $colorTxtSeccionTres = get_field('colorTxtSeccionTres');
      if( have_rows('mediosTransporte') ):
        while( have_rows('mediosTransporte') ) : the_row();
          $titMedioTransporte = get_sub_field('titMedioTransporte');
          $descMedioTransporte = get_sub_field('descMedioTransporte');
          $icoTransporte = get_sub_field('icoTransporte');
          $contMediostransporte .='<div class="titMedioTransporte">'.$icoTransporte.' '.$titMedioTransporte.'</div>';
          $contMediostransporte .='<div class="contMedioTransporte">'.$descMedioTransporte.'</div>';
          $contMediostransporte .= '<hr>';
        endwhile;
      else :
      endif;




  $contTeatro = '';

  $contTeatro .= '';


    echo $contTeatro;

  ?>

  <section style="background-color: <?php echo $fondoSeccionUno; ?>; color:<?php echo $colorTxtSeccionUno; ?>">
    <div class="container py-5">
      <div class="row justify-content-center">
        <div class="col-12 col-md-4 text-center mb-2">
          <h4><?php echo $titDesatcadoUno; ?></h4>
          <?php echo $desDestacadoUno; ?>
          <a class="btn btnAzul" href="#vermapa">ver mapa</a>
        </div>
        <div class="col-12 col-md-4 text-center mb-2">
          <?php echo $horarioTeatro; ?>
        </div>
      </div>
    </div>
  </section>
  <a name="vermapa"></a>
   <section style="background-color: <?php echo $fondoSeccionDos; ?>; color:<?php echo $colorTxtSeccionDos; ?>">
     <div class="container py-5">
       <div class="row">
         <div class="col-12"><h3><?php echo $titComoLlegar; ?></h3></div>
         <div class="col-12 col-md-6">

           <?php echo $iframeMapa; ?>
         </div>
         <div class="col-12 col-md-6">

           <?php echo $contMediostransporte; ?>
         </div>
       </div>
     </div>
   </section>
    <section style="background-color: <?php echo $fondoSeccionTres; ?>; color:<?php echo $colorTxtSeccionTres; ?>">
      <div class="container py-5">
        <div class="row">
          <div class="col-12">
            <h3><?php echo $titInfoTeatro; ?></h3>
            <?php echo $infoTeatro; ?>
          </div>
        </div>
      </div>
    </section>

  <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Mapa</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <?php echo $iframeMapa; ?>
        </div>
      </div>
    </div>
  </div>
  @endwhile
@endsection

