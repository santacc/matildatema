{{--
  Template Name: Template Equipo Creativo
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header')
  <?php

  $logoSecciom      = get_field('logoSecciom');
  $fondoSeccion     = get_field('fondoSeccion');
  $labelProduccion  = get_field('labelProduccion');
  $nombreProduccion = get_field('nombreProduccion');
  $labelDireccion   = get_field('labelDireccion');
  $nombreDireccion  = get_field('nombreDireccion');

  $labelProductoresEjecutivos   = get_field('labelProductoresEjecutivos');
  $nombreProductoresEjecutivos  = get_field('nombreProductoresEjecutivos');
  $labelProductores             = get_field('labelProductores');
  $nombreProductores            = get_field('nombreProductores');
  $contPuesto           = '';

  if( have_rows('EquipoCreativo') ):
    while( have_rows('EquipoCreativo') ) : the_row();
      $puestoEquipo = get_sub_field('puestoEquipo');
      $nombreEquipo = get_sub_field('nombreEquipo');
      $contPuesto   .= '<div class="col-11 col-md-4 mb-4 px-4 text-center">';
      $contPuesto   .= '<div class="puestoEquipo">';
      $contPuesto   .= $puestoEquipo;
      $contPuesto   .= '</div>';
      $contPuesto   .= '<div class="nombreEquipo">';
      $contPuesto   .= $nombreEquipo;
      $contPuesto   .= '</div>';
      $contPuesto   .= '</div>';


    endwhile;
  else :
  endif;


  $contEquipoCreativo = '';

  $contEquipoCreativo .= '<section style="background-image: url('.$fondoSeccion["url"].'); color: #ffffff;  padding: 20px 0; background-size: cover; background-position: bottom"><div class="container py-2" >
    <div class="row justify-content-center">
           <div class="col-11 col-md-6 mb-4 px-4 text-center">
                <img src="'. $logoSecciom["url"].'" style="max-width: 300px; width: 100%">
            </div>
        </div>
        <div class="row justify-content-center">
           <div class="col-11 col-md-10 mb-4 px-4 text-center">
                <div class="puestoEquipo">
                    '.$labelProduccion.'
                </div>
                <div class="nombreEquipo">
                    '.$nombreProduccion.'
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
           <div class="col-11 col-md-4 mb-4 px-4 text-center">
            <div class="puestoEquipo">
               '.$labelDireccion.'
            </div>
            <div class="nombreEquipo">
                '.$nombreDireccion.'
            </div>
           </div>
        </div>

    <div class="row justify-content-center">'.$contPuesto.'</div>
<div class="row justify-content-center">
           <div class="col-11 col-md-10 mb-4 px-4 text-center">
                <div class="puestoEquipo">
                    '.$labelProductoresEjecutivos.'
                </div>
                <div class="nombreEquipo">
                   '.$nombreProductoresEjecutivos.'
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
           <div class="col-11 col-md-10 mb-4 px-4 text-center">
            <div class="puestoEquipo" >
                '.$labelProductores .'
            </div>
            <div class="nombreEquipo">
                '.$nombreProductores.'
            </div>
           </div>
        </div>
  </div></section>';


  echo $contEquipoCreativo;

  ?>
  @endwhile
@endsection

