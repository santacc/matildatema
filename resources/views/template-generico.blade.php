{{--
  Template Name: Template Generico
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
    @include('partials.page-header')
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.content-page')
      </div>

    </div>

  </div>

  @endwhile
@endsection
