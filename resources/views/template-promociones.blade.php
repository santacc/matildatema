{{--
  Template Name: Template Promociones
--}}

@extends('layouts.app')

@section('content')

  <?php


  $args = [
    'post_type' => 'promocion',
    'posts_per_page' => -1,


  ];

  $query = new WP_Query( $args );
  $contPromociones = '';
  ?>
  @while($query->have_posts()) @php $query->the_post() @endphp

  <?php

  $bannerPromocion = get_field('bannerPromocion');
  $enlacePromocion = get_field('enlacePromocion');
  $contPromociones .= '<div class="col-12 col-md-5 mb-3"><a href="'. $enlacePromocion .'"><img src="'. $bannerPromocion["url"] .'" width="100%"></a></div>';
  ?>

  @endwhile

<section style="background-image: url(/wp-content/uploads/2019/12/fondo-grupos-peq.jpg); background-size: cover;">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="page-header">
          <h1>PROMOCIONES</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="container pt-1 pb-5">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10 text-center py-3">
        <h3>¡Disfruta de acceso exclusivo solo hoy por estar disfrutando de GREASE!</h3>
      </div>
    </div>
  <div class="row justify-content-center">
<?php echo $contPromociones; ?>
  </div>
  </div>
</section>



@endsection



