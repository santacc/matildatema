{{--
  Template Name: Template FAQ
--}}
<?php
// preguntas genericas
  $tituloGenerica = get_field('tituloFAQGenericas');
  // Check rows exists.
    $contenedorGenericas = '';
    if( have_rows('faqGenericas') ):
        $i =  0;
        while( have_rows('faqGenericas') ) : the_row();
         $preguntaGenerica = get_sub_field('preguntaGenerica');
         $respuestaGenerica = get_sub_field('respuestaGenerica');
         $contenedorGenericas .= '<h2 class="accordion-header" id="flush-headingOne" >
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse'.$i.'" aria-expanded="false" aria-controls="flush-collapse'.$i.'">
                                  '. $preguntaGenerica .'
                                </button>
                                </h2>';
        $contenedorGenericas .= '<div id="flush-collapse'.$i.'" class="accordion-collapse collapse" aria-labelledby="flush-heading'.$i.'" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body" style="background-color: black">
                '.$respuestaGenerica.'
              </div>
            </div>';

            $i++;
        endwhile;
    else :
    endif;

$tituloSobreTeatro = get_field('tituloFAQTeatro');
// Check rows exists.
$contenedorSobreTeatro = '';
if( have_rows('faqSobreTeatro') ):
  $j =  $i  ;
  while( have_rows('faqSobreTeatro') ) : the_row();
    $preguntaSobreTeatro = get_sub_field('preguntaSobreTeatro');
    $respuestaSobreTeatro = get_sub_field('respuestaSobreTeatro');
    $contenedorSobreTeatro .= '<h2 class="accordion-header" id="flush-headingOne" >
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse'.$i.'" aria-expanded="false" aria-controls="flush-collapse'.$i.'">
                                  '. $preguntaSobreTeatro .'
                                </button>
                                </h2>';
    $contenedorSobreTeatro .= '<div id="flush-collapse'.$i.'" class="accordion-collapse collapse" aria-labelledby="flush-heading'.$i.'" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body" style="background-color: black">
                '.$respuestaSobreTeatro.'
              </div>
            </div>';

    $i++;
  endwhile;
else :
endif;



  ?>

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
    @include('partials.page-header')
      </div>
    </div>
  </div>
    @include('partials.content-page')
  <div class="container my-5">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10">
        <div class="accordion accordion-flush" id="accordionFlushExample">
          <div class="accordion-item">
            <?php echo $contenedorGenericas; ?>
          </div>
        </div>
      </div>


    </div>
  </div>
  <!--
  <div class="container my-5">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10">
      <h3><?php echo $tituloSobreTeatro; ?></h3>
      <div class="accordion accordion-flush" id="accordionFlushExampleDos">
        <div class="accordion-item">
          <?php echo $contenedorSobreTeatro; ?>
        </div>
      </div>
      </div>
    </div>
  </div>
-->
  <section class="destacadoHome" style="padding: 0.1rem 0; background-image: url(/wp-content/themes/temamusicales/dist/images/FondoEquipoCreativo.jpg); background-size: cover; background-position: center"></section>

  @endwhile
@endsection



