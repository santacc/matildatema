{{--
       Template Name: Template Elenco Listado
     --}}

@extends('layouts.app')

@section('content')
  <?php
  $args = [
    'post_type' => 'actor',
    'posts_per_page' => -1
  ];
  $loop = new WP_Query( $args )
  ?>

  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.page-header')
      </div>
    </div>
  </div>
  <div class="container mt-5">
    <div class="row justify-content-center">
      <div class="col-sm-6 col-md-4 mb-5 text-center">
        <h3 class="nomPersonaje">Matilda</h3>
          <div class="titNomActor">Julia Awad</div>
          <div class="titNomActor">Daniela Berezo</div>
          <div class="titNomActor">Valentina Cachimbo</div>
          <div class="titNomActor">Laura Centella</div>
          <div class="titNomActor">Julieta Cruz</div>
          <div class="titNomActor">Otilia M. Domínguez</div>
          <div class="titNomActor">Rocío Zarraute</div>
        </div>
      </div>

  <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Trunchbull</h3>
          <div class="titNomActor">Oriol Burés</div>
          <div class="titNomActor">Daniel Orgaz</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Honey</h3>
        <div class="titNomActor">Allende Blanco</div>
        <div class="titNomActor"><span class="tipoAlternante">Alternante</span> Lucía Ambrosini</div>
      </div>
    </div>
  <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Mr. Wormwood</h3>
        <div class="titNomActor">Héctor Carballo</div>
        <div class="titNomActor"><span class="tipoAlternante">Alternante</span> Natxo Núñez</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Mrs. Wormwood</h3>
        <div class="titNomActor">Mary Capel</div>
        <div class="titNomActor"><span class="tipoAlternante">Alternante</span> Pepa Lucas</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Mrs. Phelps</h3>
        <div class="titNomActor">Pepa Lucas</div>
        <div class="titNomActor"><span class="tipoAlternante">Alternante</span> Natalie Pinot</div>
      </div>
    </div>
  <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Lavender</h3>
        <div class="titNomActor">Marina Cobo</div>
        <div class="titNomActor">Noa García-Navas</div>
        <div class="titNomActor">Esther Gutiérrez</div>
        <div class="titNomActor">Sofía Nieto</div>
        <div class="titNomActor">Carla Olmo</div>
        <div class="titNomActor">Marta de Toro</div>
        <div class="titNomActor">Berta del Val</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Bruce</h3>
        <div class="titNomActor">Roberto Arroba</div>
        <div class="titNomActor">Nicolás Camacho</div>
        <div class="titNomActor">Conrado Centenera</div>
        <div class="titNomActor">Manuel González</div>
        <div class="titNomActor">David Herrero</div>
        <div class="titNomActor">Nicolás Ramos</div>
        <div class="titNomActor">Oliver Rivero</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Susan</h3>
        <div class="titNomActor">María Aparicio</div>
        <div class="titNomActor">Emma Díaz</div>
        <div class="titNomActor">Irene Gallego</div>
        <div class="titNomActor">Paula Jerez</div>
        <div class="titNomActor">Valeria Langarica</div>
        <div class="titNomActor">Youlan Lin</div>
      </div>
    </div>
    <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Amanda</h3>
        <div class="titNomActor">Cloe A. López</div>
        <div class="titNomActor">Claudia Busquier</div>
        <div class="titNomActor">Nadia Cano</div>
        <div class="titNomActor">Michelle González</div>
        <div class="titNomActor">Sofía Jerez</div>
        <div class="titNomActor">Ángela Pigueiras</div>
      </div>
      <div class="col-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Alice</h3>
        <div class="titNomActor">Ana Casado</div>
        <div class="titNomActor">Nerea Centella</div>
        <div class="titNomActor">Hada Gutiérrez</div>
        <div class="titNomActor">Naiara Herrera</div>
        <div class="titNomActor">Alba Nájera</div>
        <div class="titNomActor">Lara Pierozzi</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Hortensia</h3>
        <div class="titNomActor">Adriana García</div>
        <div class="titNomActor">Alicia Jerez</div>
        <div class="titNomActor">Bárbara Martínez</div>
        <div class="titNomActor">Patricia Plaza</div>
        <div class="titNomActor">Mariana Rincón</div>
        <div class="titNomActor">Sarah Urbina</div>
      </div>
    </div>
    <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Eric</h3>
        <div class="titNomActor">Juan Diego Álvarez</div>
        <div class="titNomActor">Pablo Grifé</div>
        <div class="titNomActor">Eneko Haren</div>
        <div class="titNomActor">Ciro Pérez</div>
        <div class="titNomActor">Gabriel Sanz</div>
        <div class="titNomActor">Hugo Valdivielso</div>
        <div class="titNomActor">David Wentworth</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Chris</h3>
        <div class="titNomActor">Reisha Figuración</div>
        <div class="titNomActor">Nerea Íñigo</div>
        <div class="titNomActor">Harriet Jovani</div>
        <div class="titNomActor">Martina Naranjo</div>
        <div class="titNomActor">Marina Ortiz</div>
        <div class="titNomActor">Alba Silva</div>

      </div>
    </div>
    <div class="row justify-content-center pt-2">
      <div class="col-sm-6 col-md-4 mb-5">
        <h3 class="nomPersonaje">Elenco</h3>
        <div class="titNomActor">Lucía Ambrosini</div>
        <div class="titNomActor">Oriol Anglada</div>
        <div class="titNomActor">Rubén Buika</div>
        <div class="titNomActor">Ana Escrivá</div>
        <div class="titNomActor">María Gago</div>
        <div class="titNomActor">Víctor González</div>
        <div class="titNomActor">Flor Lopardo</div>
        <div class="titNomActor">Enric Marimón</div>
        <div class="titNomActor">Marina Martín</div>
        <div class="titNomActor">Javier Santos</div>
      </div>
      <div class="col-sm-6 col-md-4 mb-5">
      <h3 class="nomPersonaje">Swing</h3>
      <div class="titNomActor">Alex Chavarri</div>
      <div class="titNomActor">Álvaro Cuenca</div>
      <div class="titNomActor">Joana Quesada</div>
      <div class="titNomActor">Carmen Soler</div>
    </div>
    </div>
  <div class="row mb-5">
    <div class="col-12 text-center">
      *Por personaje y en orden alfabético de apellido
    </div>
  </div>
    </div>
 


     @endwhile
@endsection


