@extends('layouts.app')


@section('content')
   {{-- header con el logo / opcion de poner video de slider --}}
    @include('partials.front-page.headerHome')
   {{-- Premios y opiniones --}}
   @include('partials.front-page.premiosopiniones')
   {{-- Recordatorio de entradas --}}
   @include('partials.front-page.ventajasHome')
   @include('partials.front-page.recordatorioEntradas')


  <a name="sinopsisDiv"></a>
   {{-- Sinopsis del musical --}}
  @include('partials.front-page.sinopsisHome')

   {{-- Equipo creativo --}}
   {{-- Seccion Ayuda HOME --}}
   @include('partials.front-page.ayuda')



@endsection

<style>
  span {
    display: inline-block;
  }
</style>
