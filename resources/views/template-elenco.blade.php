{{--
       Template Name: Template Elenco
     --}}

@extends('layouts.app')

@section('content')
  <?php
  $args = [
    'post_type' => 'actor',
    'posts_per_page' => -1
  ];
  $loop = new WP_Query( $args )
  ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.page-header')
      </div>
      <div class="container">
        <div class="row">
          @while($loop->have_posts()) @php $loop->the_post() @endphp

          @include('partials.content-actor')
          @endwhile
        </div>
      </div>

      @endsection



<!--
<?php
/*
    $personajesPrincipales = '';

    if( have_rows('personaje') ){
      $personajesPrincipales .= '<div class="row justify-content-center">';
        while( have_rows('personaje') ) {
          the_row();
          $nombrePersonaje = get_sub_field('nombrePersonaje');
          $tipoPersonaje = get_sub_field('tipoPersonaje');
          $numeroActores = get_sub_field('numeroActores');
          $actores = get_sub_field('actores');
          if($tipoPersonaje == 'principal') {
            $personajesPrincipales .=  '<div class="col-6 p-5"><div class="row justify-content-center"><h3 class="nomPersonaje">'.$nombrePersonaje.'</h3>';
            if( $actores ) {
              foreach( $actores as $actor ){
                $personajesPrincipales .= '<div class="col-6 p-3 actorGridExte" id="'. $actor->ID.'"><div class="actorGrid">';
                $featured_img_url = get_field( 'fotoActor', $actor->ID );
                $personajesPrincipales .= '<img src="'. $featured_img_url["url"] .'" width="100%">';
                $personajesPrincipales .= '<div class="titNomActor">'. get_the_title( $actor->ID ).'</div>';
                $personajesPrincipales .= '<div class="contCurriculo" id="cont_'. $actor->ID.'"><div class="container"><div class="row"><div class="col-3" style="background-image: url('.$featured_img_url["url"].' ); background-size: cover; "></div><div class="col-8" style="position: relative; background-color: black"><div class="btnCerrar" data-cierre="'. $actor->ID.'">X</div><div class="nombreActor">'.get_the_title( $actor->ID ).'</div><div class="cvActor">'.get_field( 'cvActor', $actor->ID ).'</div></div></div></div></div>';
                $personajesPrincipales .= '</div></div>';
              }
            }
            $personajesPrincipales .= '</div></div>';
          } else if($tipoPersonaje == 'secundarios') {
            $personajesPrincipales .=  '<div class="col-6 p-5"><div class="row justify-content-center"><h3 class="nomPersonaje">'.$nombrePersonaje.'</h3>';
            if( $actores ) {
              foreach( $actores as $actor ){
                $personajesPrincipales .= '<div class="col-4 p-3 actorGridExte" id="'. $actor->ID.'"><div class="actorGrid">';
                $featured_img_url = get_field( 'fotoActor', $actor->ID );
                $personajesPrincipales .= '<img src="'. $featured_img_url["url"] .'" width="100%">';
                $personajesPrincipales .= '<div class="titNomActor">'. get_the_title( $actor->ID ).'</div>';
                $personajesPrincipales .= '<div class="contCurriculo" id="cont_'. $actor->ID.'"><div class="container"><div class="row"><div class="col-3" style="background-image: url('.$featured_img_url["url"].' ); background-size: cover; "></div><div class="col-8" style="position: relative; background-color: black"><div class="btnCerrar" data-cierre="'. $actor->ID.'">X</div><div class="nombreActor">'.get_the_title( $actor->ID ).'</div><div class="cvActor">'.get_field( 'cvActor', $actor->ID ).'</div></div></div></div></div>';
                $personajesPrincipales .= '</div></div>';
              }
            }
            $personajesPrincipales .= '</div></div>';
          } else if($tipoPersonaje == 'elenco') {
            $personajesPrincipales .=  '<div class="col-12 p-5"><div class="row justify-content-center"><h3 class="nomPersonaje">'.$nombrePersonaje.'</h3>';
            if( $actores ) {
              foreach( $actores as $actor ){
                $personajesPrincipales .= '<div class="col-2 p-3 actorGridExte" id="'. $actor->ID.'"><div class="actorGrid">';
                $featured_img_url = get_field( 'fotoActor', $actor->ID );
                $personajesPrincipales .= '<img src="'. $featured_img_url["url"] .'" width="100%">';
                $personajesPrincipales .= '<div class="titNomActor">'. get_the_title( $actor->ID ).'</div>';
                $personajesPrincipales .= '<div class="contCurriculo" id="cont_'. $actor->ID.'"><div class="container"><div class="row"><div class="col-3" style="background-image: url('.$featured_img_url["url"].' ); background-size: cover; "></div><div class="col-8" style="position: relative; background-color: black"><div class="btnCerrar" data-cierre="'. $actor->ID.'">X</div><div class="nombreActor">'.get_the_title( $actor->ID ).'</div><div class="cvActor">'.get_field( 'cvActor', $actor->ID ).'</div></div></div></div></div>';
                $personajesPrincipales .= '</div></div>';
              }
            }
            $personajesPrincipales .= '</div></div>';

        }
      }
    } else {

    }
$personajesPrincipales .= '</div>';
*/
?>
