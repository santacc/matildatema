{{--
  Template Name: Template Programa Especial
--}}
<?php
$logoEspecial = get_field('logoEspecial');
$textoEspecial = get_field('textoEspecial');
$imagenEspecial = get_field('imagenEspecial');

?>
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container mb-5">
    <div class="row">
      <div class="col-12 col-md-6 text-center">
        <img src="<?php echo $imagenEspecial["url"]; ?>" width="80%">

      </div>
      <div class="col-12 col-md-6 text-center" style="align-content: center; align-items: center; align-self: center;">
          <div class="page-header">
            <h1><?php echo $textoEspecial; ?>  <img src="<?php echo $logoEspecial["url"]; ?>" style="max-width: 100px" > de Grease!</h1>
          </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12">
       <?php
        $codigoFlip = get_field('codigoProgramaMano','option');
        echo do_shortcode($codigoFlip); ?>
      </div>
    </div>
  </div>

  <?php
        $textoIntroProgramaMano = get_field('textoIntroProgramaMano','option');
        $fondoBanners = get_field('fondoBanners','option');

  ?>

  <section class="py-5 mt-5" style="background-image: url(<?php echo $fondoBanners["url"]; ?>); background-size: cover;">
    <div class="container my-5">
      <div class="row">
        <div class="col-12 text-center">
          <h2><?php echo $textoIntroProgramaMano; ?></h2>

        </div>
      </div>
    </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.content-page')
      </div>

    </div>

  </div>
  </section>

  @endwhile
@endsection
