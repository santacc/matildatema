{{--
       Template Name: Template Elenco Slide
     --}}


@extends('layouts.app')

@section('content')
  <?php
  $bulletsSlide = '';
  $contDescripcion = '';
  $i = 0;
  $args = [
    'post_type' => 'actor',
    'posts_per_page' => -1
  ];
  $loop = new WP_Query( $args );
  $personajesPrincipales = '';

  ?>


            <?php
                $j=0;
                while($loop->have_posts()) {
                  $loop->the_post();
                    $idActor = get_the_ID();
                    $fotoActor = get_field('fotoActor');
                    $cvActor = get_the_excerpt(  );
                    $personajePrincipal = get_field('personajePrincipal');
                    $segundoPersonaje = get_field('segundoPersonaje');
                    $terceroPersonaje = get_field('terceroPersonaje');
                    $cuartoPersonaje = get_field('cuartoPersonaje');
                    $quintoPersonaje = get_field('quintoPersonaje');
                    $personajesPrincipales .= '<li class="glide__slide destacadoisGrid" data-position="'.$j.'"><div class="actorGridExte" id="'.  $idActor .'"><div class="actorGrid">';
                    $personajesPrincipales .= '<img src="'. $fotoActor["url"].'" width="100%">';
                    $personajesPrincipales .= '<div class="titNomActor">'. get_the_title() .'</div>';
                    $personajesPrincipales .= '</div>';
                   /* $personajesPrincipales .= '<div class="fondoPersonajes">';
                    if($personajePrincipal != '') {
                      $personajesPrincipales .= '<div class="persoPrinActor">'. $personajePrincipal .'</div>';
                    }
                    if($segundoPersonaje != '') {
                      $personajesPrincipales .= '<div class="persoSegundoActor">'. $segundoPersonaje .'</div>';
                    }
                    if($terceroPersonaje != '') {
                      $personajesPrincipales .= '<div class="persoTerceroActor">'. $terceroPersonaje .'</div>';
                    }
                    if($cuartoPersonaje != '') {
                      $personajesPrincipales .= '<div class="persoCuartoActor">'. $cuartoPersonaje .'</div>';
                    }
                    if($quintoPersonaje != '') {
                      $personajesPrincipales .= '<div class="persoQuintoActor">'. $quintoPersonaje .'</div>';
                    }*/
                    $personajesPrincipales .= '</li>';
                    $destacadosHome = '';
                    $destacadosHome .= $personajesPrincipales;

                    $contDescripcion .= '<div class="contCurriculo" id="cont_'. $idActor .'"><div class="contFlechas"><div class="flechaExt"></div><div class="flechaInt"></div></div>
                                                <div class="container">
                                                    <div class="row justify-content-center">
                                                        <div class="col-12 col-md-8" style="border-top: 1px solid #fff; position: relative;">
                                                            <div class="nombreActor">'. get_the_title() .'</div>
                                                            <div class="nombreActor" style="color: #ee5754; font-size: 1.1rem; font-weight: bold; ">'. $personajePrincipal .'</div>
                                                            <div class="cvActor">'.$cvActor.'</div>
                                                            <a href="'. get_the_permalink().'" class="btn btn-outline-blanco" style="width: 100%; margin-top: 2%; width: 50%; margin: 2% auto; display: block;">Ver más</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>';
                    $i++;
                    $bulletsSlide .= '<button class="glide__bullet" data-glide-dir="='.$i.'"><</button>';
                    $j++;
            }

            ?>

  <div class="container mb-5">
    <div class="row">
      <div class="col-12">
        <div class="page-header">
        <h1 style="text-align: center; margin: 2% 0;">ELENCO</h1>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="glideDestacados" style="width: 100%; margin: 0 auto">
          <div class="glide__track" data-glide-el="track" >
            <ul class="glide__slides">
<?php  echo $destacadosHome; ?>
          </ul>

        </div>
        <div class="glide__arrows" data-glide-el="controls">
          <button class="glide__arrow glide__arrow--left" data-glide-dir="<"> <img src="/wp-content/themes/temamusicales/dist/images/flecha_izq.png"> </button>
          <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><img src="/wp-content/themes/temamusicales/dist/images/flecha_der.png"> </button>
        </div>
        <!-- <div class="glide__bullets" data-glide-el="controls[nav]">
          <?php echo $bulletsSlide; ?>
        </div> -->
      </div>
        </div>
        <?php echo $contDescripcion; ?>
      </div>
  </div>

<section style="background-image: url(/wp-content/uploads/2021/12/fondo_libros_ejemplo.jpg); background-position: bottom; background-size: cover">
    <div class="container text-center mt-4 py-5" >
      <div class="row mb-5">
        <div class="col-12 text-center">
          <h2>MUSICOS</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 centrado">
          <div class="puestoBanda">DIR. MUSICAL</div>
          <div class="nomBanda">JOAN MIQUEL PEREZ</div>
        </div>
        <div class="col-sm-12 centrado">
          <div class="puestoBandaSup">Piano / Director Musical Suplente</div>
          <div class="nomBandaSup">ALEX LARRAGA</div><p></p>
        </div>
        <div class="col-sm-12 centrado">
          <div class="puestoBandaSup">Piano / Director Musical Suplente</div>
          <div class="nomBandaSup">JACOB SUREDA</div><p></p>
        </div>
      </div>
      <div class="row my-5">
        <div class="col-sm-4 centrado">
          <div class="puestoBanda">SAXO TITULAR</div>
          <div class="nomBanda">JORDI BALLARIN</div>
          <div class="puestoBandaSup">SAXO SUPLENTE</div>
          <div class="nomBandaSup">AARON  POZON</div>
          <div class="puestoBandaSup">SAXO SUPLENTE</div>
          <div class="nomBandaSup">SERGIO BIENZOBAS
          </div>
        </div>
        <div class="col-sm-4 centrado">
          <div class="puestoBanda">TROMPETA TITULAR</div>
          <div class="nomBanda">JUANLU DE LA TORRE</div>
          <div class="puestoBandaSup">TROMPETA SUPLENTE</div>
          <div class="nomBandaSup">FERNANDO HURTADO</div>
          <div class="puestoBandaSup">TROMPETA SUPLENTE</div>
          <div class="nomBandaSup">FCO JAVIER AREVALO</div>
        </div>


        <div class="col-sm-4 centrado">
          <div class="puestoBanda">BATERIA TITULAR</div>
          <div class="nomBanda">MARIO CARRION</div>
          <div class="puestoBandaSup">BATERIA SUPLENTE</div>
          <div class="nomBandaSup">ENRIC CASTELLO</div>
          <div class="puestoBandaSup">BATERIA SUPLENTE</div>
          <div class="nomBandaSup">ALEX ZARZALEJO</div>
        </div>
      </div>
      <div class="row my-5">
        <div class="col-sm-6 centrado">
          <div class="puestoBanda">BAJO TITULAR</div>
          <div class="nomBanda">DANIEL CASIELLES</div>
          <div class="puestoBandaSup">BAJO SUPLENTE</div>
          <div class="nomBandaSup">JACOB REGUILON</div>
        </div>
        <div class="col-sm-6 centrado">
          <div class="puestoBanda">GUITARRA TITULAR</div>
          <div class="nomBanda">QUIQUE BERRO-GARCIA</div>
          <div class="puestoBandaSup">GUITARRA SUPLENTE</div>
          <div class="nomBandaSup">JAVIER SANTOS</div>
        </div>
        </div>
      </div>
    </div>
</section>
@endsection




