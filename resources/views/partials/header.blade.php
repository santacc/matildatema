<header class="banner">
  <div class="container-fluid" style="max-width: 1550px">
    <nav class="navbar navbar-expand-md mr-auto animated fadeInUp" role="navigation">
      <?php
      $imagenLogo = get_field('logoHeader','option');
      $imagenEntradas = get_field('btnEntradas','option');
      ?>
        <a class="brand " href="{{ home_url('/') }}">
          <img src="<?php echo $imagenLogo["url"] ;?>" alt="logo Grease el musical" class="destello logoHeader">
        </a>
      <!-- <a class="brand mr-auto" href="{{ home_url('/') }}"><img src="{{ $imagenLogo['url'] }}" class="logoHeader" alt="Logo SOM Academy"></a> -->
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu($primarymenu) !!}
      @endif
        <a class="btnEntradas" href="/entradas/"><img src="{{ $imagenEntradas['url'] }}" class="logoEntradas" alt="{{ $imagenEntradas['alt'] }}"></a>

    </nav>


  </div>
</header>
<button class="hamburger hamburger--spin" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
<div class="menuMovil">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu($primarymenu) !!}
  @endif
</div>

