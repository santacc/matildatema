<?php

if( have_rows('seccionRecorEntradas','options') ):
  while( have_rows('seccionRecorEntradas','options') ):
    the_row();
    $activeSeccion = get_sub_field('activeSeccion');
    $confSeccionesHome = get_sub_field('confSeccionesHome');
    $tipoFondo = get_sub_field('tipoFondo');
    $fondoColor = get_sub_field('fondoColor');
    $imageFondo = get_sub_field('imageFondo');
    $urlVideo = get_sub_field('urlVideo');
    $paddingY = get_sub_field('paddingY');
    $colorTexto = get_sub_field('colorTexto');
    $imagenRecordatorioEntradas = get_sub_field('imgEntradas');
    $txtRecordatorioEntradas = get_sub_field('txtRecordatorioEntradas');
    $txtRecordatorioDos = get_sub_field('txtRecordatorioDos');
    $txtBotonEntradas = get_sub_field('txtBotonEntradas');
    $txtEnlaceBoton = get_sub_field('txtEnlaceBoton');
  endwhile;
endif;
$contVentajas = '';
if( have_rows('ventajas','options') ):
  while( have_rows('ventajas','options') ):
    the_row();
    // Get sub field values.
    $imagenIcoVentajas = get_sub_field('imagenIcoVentajas');
    $textoVentajas = get_sub_field('textoVentajas');
    $contVentajas .='<div class="col-6 col-md-4 text-center">
        <a href="'.$txtEnlaceBoton.'"><img src="'.$imagenIcoVentajas["url"].'" class="imagenVentajaIcono"></a>
        <p class="parrafVentajaTxt">'.$textoVentajas.'</p>
      </div>';
  endwhile;
endif;
$textoFinalVentajas = get_field('textoFinalVentajas','options');
$textoIntroVentajas = get_field('textoIntroVentajas','options');



?>

<section id="secVentajas" class="py-5 my-5">
  <h3 class="tituloVentajas"><?php echo $textoIntroVentajas;?></h3>
  <div class="container">
    <div class="row">
      <?php echo $contVentajas; ?>
    </div>
    <div class="row p-0 m-0">
      <div class="col-12 text-center p-0 m-0"><p class="textoInfVentajas"><?php echo $textoFinalVentajas;?></p></div>
    </div>
    <div class="row">
      <div class="col-12 p-0">

        <div class="enlaceBtn"><a class="btn btnRecordatorio" href="<?php echo $txtEnlaceBoton;?>" ><?php echo $txtBotonEntradas; ?></a></div>
      </div>
    </div>

  </div>
</section>
