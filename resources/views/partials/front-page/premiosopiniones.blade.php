<?php
  $paddingY = 0;
  if( have_rows('seccionOpiniones','options') ):
    while( have_rows('seccionOpiniones','options') ):
      the_row();
      $activeSeccion = get_sub_field('activeSeccion');
      $confSeccionesHome = get_sub_field('confSeccionesHome');
      $tipoFondo = get_sub_field('tipoFondo');
      $fondoColor = get_sub_field('fondoColor');
      $imageFondo = get_sub_field('imageFondo');
      $urlVideo = get_sub_field('urlVideo');
      $paddingY = get_sub_field('paddingY');
      $colorTexto = get_sub_field('colorTexto');
    endwhile;
  endif;
$textInferiorPremios = get_field('textInferiorPremios','options');
$imgPremios = get_field('imgPremios','options');
$svgLetras = get_field('svgLetras','options');
$imagenLetras =get_field('imagenLetras','options');
$contPremios = '<div class="row mb-5 mt-4 justify-content-center">';
if( have_rows('premiosListado','options') ):
  while( have_rows('premiosListado','options') ):
    the_row();
    $nomPremio = get_sub_field('nomPremio');
    $exPremioPremio = get_sub_field('exPremioPremio');
    $contPremios .= '<div class="col-6 col-md-3 text-center py-5 listPremio" style="background-image: url('.$imgPremios["url"].');">
                        <div class="nomPremio">'.$nomPremio.'</div>
                        <div class="explicacion">'.$exPremioPremio.'</div>
                </div>';
  endwhile;
endif;
$contPremios .= '</div>';
  $contPremiosOpiniones = '';


  if($tipoFondo == 'color') {
    $contPremiosOpiniones .= '<section id="contPremios" style="background-color: '. $fondoColor .'; color: '.$colorTexto.';  padding: '.$paddingY.'px 0 0 0; ">';
  } else if($tipoFondo == 'imagen') {
    $contPremiosOpiniones .= '<section id="contPremios" style="background-image: url('.$imageFondo["url"] .'); color: '.$colorTexto.';  padding: '.$paddingY.'px 0 0 0; background-size: cover; background-position: bottom">';
  }

  $contPremiosOpiniones .= '<div class="container">
                                <div class="row justify-content-center mb-5">
                                    <div class="col-12 col-md-8 text-center line-drawing-demo">
                                    <img src="'.$imagenLetras["url"].'" width="100%" style="max-width: 600px">
                                    </div>
                                </div>
                                '.$contPremios.'
                            </div>

  <div class="textInferior" >'.$textInferiorPremios.'</div>';
  $contPremiosOpiniones .=  '</section>';

  if ($activeSeccion != '') {
  echo $contPremiosOpiniones;
  }
  ?>


