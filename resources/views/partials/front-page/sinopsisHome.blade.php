<?php


$paddingY = 0;
if( have_rows('secApartadoSinopsis','options') ):
  while( have_rows('secApartadoSinopsis','options') ):
    the_row();
    $activeSeccion = get_sub_field('activeSeccion');
    $confSeccionesHome = get_sub_field('confSeccionesHome');
    $tipoFondo = get_sub_field('tipoFondo');
    $fondoColor = get_sub_field('fondoColor');
    $imageFondo = get_sub_field('imageFondo');
    $urlVideo = get_sub_field('urlVideo');
    $paddingY = get_sub_field('paddingY');
    $colorTexto = get_sub_field('colorTexto');
    $tituloSinopsis = get_sub_field('tituloSinopsis');

    $imagenSinopsis = get_sub_field('imagenSinopsis');
    $textoSinopsis = get_sub_field('txtSinopsis');

    $fondoSupSinopsis = get_sub_field('fondoSupSinopsis');
    $fondoInfSinopsis = get_sub_field('fondoInfSinopsis');
  endwhile;
endif;

$contSinopsisHome= '';

if($tipoFondo == 'color') {
  $contSinopsisHome .= '<section id="sinopsis" style="background-color: '. $fondoColor .'; color: '.$colorTexto.';  padding: '.$paddingY.'px 0; ">';
} else if($tipoFondo == 'imagen') {
  $contSinopsisHome .= '<section id="sinopsis" style="color: '.$colorTexto.';  padding: '.$paddingY.'px 0;">';
}

  $contSinopsisHome .= '<div class="container pt-2">
      <div class="row align-content-center justify-content-center">
        <div class="col-8 col-md-6 text-center" >
          <h3 class="tituloSeccion">'.$tituloSinopsis.'</h3>
        </div>
      </div>
    <div class="row mt-4 align-content-center justify-content-center contSinopsis" style="background-image: url('.$fondoSupSinopsis["url"].');">
      <div class="col-12 col-md-8 text-center">
        <div class="txtSinopsis">'. $textoSinopsis.'</div>
      </div>
    </div>
    <div class="row mt-4 align-content-center justify-content-center contLibros" style="background-image: url('.$fondoInfSinopsis["url"].');">
        <div class="col-12 ">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_amarillo.png" class="librosVoladores" id="libro1">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_azul.png" class="librosVoladores" id="libro2">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_derecha_amarillo.png" class="librosVoladores" id="libro3">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_morado_uno.png" class="librosVoladores" id="libro4">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_rojo.png" class="librosVoladores" id="libro5">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_rosa_izquierda.png" class="librosVoladores" id="libro6">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_rosa.png" class="librosVoladores" id="libro7">
        <img src="/wp-content/themes/matildatema/dist/images/libros/libro_verde.png" class="librosVoladores" id="libro8">
      </div>
  </div>
    </div>';

  $contSinopsisHome .= '</section>';
  if ($activeSeccion != '') {
    echo $contSinopsisHome;
  }

  ?>

