<?php
$titAyudamos = get_field('titAyudamos','options');
  $txtAyudamos = get_field('txtAyudamos','options');
  $svgSalmandra = get_field('svgSalmandra','options');
?>
<section class="p-5" id="contAyuda">
<div class="container">
  <div class="row justify-content-center">
    <div class="col-4 col-md-2">
<?php echo $svgSalmandra; ?>
    </div>
    <div class="col-12 col-md-4 text-center conTextoAyuda">
      <h4 class="titAyuda"><?php echo $titAyudamos; ?> </h4>
      <?php echo $txtAyudamos; ?>
    </div>
  </div>
</div>
</section>
