<?php
  $imagenIzquierda = get_field('imagenIzquierda','option');
  $imagenDerecha = get_field('imagenDerecha','option');
  $txtBotonHeader = get_field('txtBotonHeader','option');
  $linkBotonHeader = get_field('linkBotonHeader','option');
  $iframeVideo = get_field('iframeVideo','option');

  $mostrarDestacado = get_field('mostrarDestacado','option');
  $titDestacadoEspecial = get_field('titDestacadoEspecial','option');
  $subtitDestacadoEspecial = get_field('subtitDestacadoEspecial','option');
  $preDestacadoEspecial = get_field('preDestacadoEspecial','option');
  $listadoEspeciales='';
  $listadoEspeciales .= '<ul class="listaSeguridad">';
  if( have_rows('listadoEspeciales','option') ):
    while( have_rows('listadoEspeciales','option') ):
      the_row();
      $itemListadoEspeciales = get_sub_field('itemListadoEspeciales');
      $listadoEspeciales .= '<li>'.$itemListadoEspeciales.'</li>';
    endwhile;
  endif;
  $listadoEspeciales .= '</ul>';
  $txtDestacadoEspecial = get_field('txtDestacadoEspecial','option');
  $imagenEspecialDerecha = get_field('imagenEspecialDerecha','option');
  $imagenDestacadoEspecial = get_field('imagenDestacadoEspecial','option');
  $urlEspecial = get_field('urlEspecial','option');


$contenedorHeader = '<section class="sliderHome">
  <div class="container">
    <div class="row justify-content-center p-0">
        <div class="col-12 col-md-6 col-lg-5">
            <img src="'. $imagenEspecialDerecha["url"] .'" alt="'. $imagenEspecialDerecha["alt"] .'" width="90%">
        </div>
        <div class="col-12 col-md-6 col-lg-5 contseguridad">
            <h4 class="tituloCompraSeguridad">'. $titDestacadoEspecial .'</h4>
            <div class="subTitulo">'. $subtitDestacadoEspecial .'</div>
            <div class="tituloLista"> '. $preDestacadoEspecial .'</div>
              <a href="'. $urlEspecial.'" target="_blank" class="linkTarjetaSeguridad">
                    <img src="'. $imagenDestacadoEspecial["url"].'" width="70%" class="imgSeguridad">
              </a>
              '.$listadoEspeciales.'
        </div>
    </div>
  </div>


</section>';
    $verVideoHeader = get_field('verVideoHeader','option');
    $videoUrl = get_field('videoUrl','option');

    if($mostrarDestacado == 'Si') {
      echo $contenedorHeader;
    }

    if($verVideoHeader == 'Si') {
    echo '<div class="videoHome">
            <div id="botonesAudio" style="position: absolute; z-index: 999; bottom: 3%; left: 3%;">
                    <i class="fas fa-2x fa-volume-up" id="quitar-mute"></i>
                    <i class="fas fa-2x fa-volume-mute" id="mute-video"></i></div>
              <video autoplay="1" id="myVideo" loop="1"  preload="auto" muted playsinline>
                <source src="'.$videoUrl.'" type="video/mp4">
              </video>
            </div>';
     // echo '<div class="videoHome"><iframe src="https://player.vimeo.com/video/675829978?autoplay=1&loop=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="Matilda_promo_def.mov"></iframe></div>';
      // echo  '<div class="videoHome">'. do_shortcode('[rev_slider alias="slider-1"][/rev_slider]') .'</div>';
  }

?>

<!--

<audio controls>
  <source src="forelisa.mp3" type="audio/mp3">
  Tu navegador no soporta audio HTML5.
</audio>
-->
