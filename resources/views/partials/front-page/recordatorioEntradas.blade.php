<?php
$paddingY = 0;
$imgEntradas = get_field('imgEntradas','options');
if( have_rows('seccionRecorEntradas','options') ):
  while( have_rows('seccionRecorEntradas','options') ):
    the_row();
    $activeSeccion = get_sub_field('activeSeccion');
    $confSeccionesHome = get_sub_field('confSeccionesHome');
    $tipoFondo = get_sub_field('tipoFondo');
    $fondoColor = get_sub_field('fondoColor');
    $imageFondo = get_sub_field('imageFondo');
    $urlVideo = get_sub_field('urlVideo');
    $paddingY = get_sub_field('paddingY');
    $colorTexto = get_sub_field('colorTexto');
    $imagenRecordatorioEntradas = get_sub_field('imgEntradas');
    $txtRecordatorioEntradas = get_sub_field('txtRecordatorioEntradas');
    $txtRecordatorioDos = get_sub_field('txtRecordatorioDos');
    $txtBotonEntradas = get_sub_field('txtBotonEntradas');
    $txtEnlaceBoton = get_sub_field('txtEnlaceBoton');
  endwhile;
endif;

$contRecordatorioEntradas = '';
if($tipoFondo == 'color') {
  $contRecordatorioEntradas .= '<section id="recordEntradas" style="background-color: '. $fondoColor .'; color: '.$colorTexto.';  padding: 0; ">';
} else if($tipoFondo == 'imagen') {
  $contRecordatorioEntradas .= '<section id="recordEntradas" style="background-image: url('.$imageFondo["url"] .'); color: '.$colorTexto.';  padding: 0; background-size: cover; background-position: top">';
}

$contRecordatorioEntradas .= '
<div class="container">
  <div class="row">
    <div class="col-12">
      <img src="'. $imgEntradas["url"] .'" class="imgSinopsis" width="100%">
    </div>
  </div>
</div>

</section>';
$contRecordatorioEntradas .= '</section>';

if ($activeSeccion != '') {
  echo $contRecordatorioEntradas;
}
?>
