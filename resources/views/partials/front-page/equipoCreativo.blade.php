<?php
      $tiEquipoCreativo = get_field('titEquipoCreativo','option');




    $paddingY = 0;
    if( have_rows('seccionEquipoCreativo','options') ):
      while( have_rows('seccionEquipoCreativo','options') ):
        the_row();
        $activeSeccion = get_sub_field('activeSeccion');
        $confSeccionesHome = get_sub_field('confSeccionesHome');
        $tipoFondo = get_sub_field('tipoFondo');
        $fondoColor = get_sub_field('fondoColor');
        $imageFondo = get_sub_field('imageFondo');
        $urlVideo = get_sub_field('urlVideo');
        $paddingY = get_sub_field('paddingY');
        $colorTexto = get_sub_field('colorTexto');

        $contPuesto = '';
        if( have_rows('EquipoCreativo') ):
          while( have_rows('EquipoCreativo') ) : the_row();
            $puestoEquipo = get_sub_field('puestoEquipo');
            $nombreEquipo = get_sub_field('nombreEquipo');
            $contPuesto .= '<div class="col-11 col-md-4 mb-2 p-1 text-center">';
            $contPuesto .= '<div class="puestoEquipo">';
            $contPuesto .= $puestoEquipo;
            $contPuesto .= '</div>';
            $contPuesto .= '<div class="nombreEquipo">';
            $contPuesto .= $nombreEquipo;
            $contPuesto .= '</div>';
            $contPuesto .= '</div>';
          endwhile;
        else :
        endif;
      endwhile;
    endif;

    $contEquipoCreativo = '';
    //$contPremiosOpiniones .= '<section class="p-5" style="width: 100%; background-image: url(/wp-content/themes/temamusicales/dist/images/fondo_opiniones.jpg); background-size: cover; background-position: top">';

    if($tipoFondo == 'color') {
      $contEquipoCreativo .= '<section style="background-color: '. $fondoColor .'; color: '.$colorTexto.';  padding: '.$paddingY.'px 0; ">';
    } else if($tipoFondo == 'imagen') {
      $contEquipoCreativo .= '<section style="background-image: url('.$imageFondo["url"] .'); color: '.$colorTexto.';  padding: '.$paddingY.'px 0; background-size: cover; background-position: top">';
    } else {
      $contEquipoCreativo .= '<section class="py-2 my-2 conVideo"><div style="width: 100%; position:absolute; height: 100%; background-color: rgba(0,0,0,0.2)"></div>
                <div class="miPrueba"> <iframe class="videoDesk" src="'.$urlVideo.'?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
    }

    $contEquipoCreativo .= '
  <div class="container py-5" >
    <div class="row">
      <div class="col-12">
        <h3 class="tituloSeccion">'. $tiEquipoCreativo .'</h3>
      </div>
    </div>
    <div class="row justify-content-center">
        '. $contPuesto.'
    </div>
  </div>';
    $contEquipoCreativo .= '</section>';
if ($activeSeccion != '') {
  echo $contEquipoCreativo;
}
?>

