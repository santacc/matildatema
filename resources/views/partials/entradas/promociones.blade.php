<?php

$paddingY = 0;
if( have_rows('destacadosPromociones') ):
  while( have_rows('destacadosPromociones') ):
    the_row();
    // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');

    $paddingY = get_sub_field('paddingY');
  endwhile;
endif;


$contenedorPromociones = '';
if($tipoFondoSec == 'color') {
  $contenedorPromociones .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
} else if($tipoFondoSec == 'imagen') {
  $contenedorPromociones .= '<section class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
} else {
  $contenedorPromociones .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';">';
  /* $contenedorTipoVenta .= '<section class="py-2 my-2 conVideo"><div style="width: 100%; position:absolute; height: 100%; background-color: rgba(0,0,0,0.2)"></div>
     <div class="miPrueba"> <iframe class="videoDesk" src="'.$urlVideoSec.'?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';*/
}

$contenedorPromociones .= '<div class="container"><div class="row">';

if( have_rows('contDestPromociones','options') ):
  while( have_rows('contDestPromociones','options') ): the_row();
    $titDestPromocion = get_sub_field('titDestPromocion');
    $textDestPromocion = get_sub_field('textDestPromocion');
    $fondoDestPromocion = get_sub_field('fondoDestPromocion');
    $contenedorPromociones .= '<div class="col-12 col-md-4 mb-3">
        <div class="p-5 cuadroFondo" style="background-image: url('. $fondoDestPromocion["url"] .');">
          <h3 class="tituloSeccion"> '.$titDestPromocion.'</h3>
          '.$textDestPromocion.'
        </div>
      </div>';
  endwhile;
endif;
$contenedorPromociones .='</div></div></section>';


$titDestGrupo = get_field('titDestGrupo','options');
$textDestGrupo = get_field('textDestGrupo','options');
$fondoDestGrupo = get_field('fondoDestGrupo','options');

?>
<section id="secGrupo" style="background-image: url(<?php echo $fondoDestGrupo["url"]; ?> );">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-6">
        <h3 style="font-weight: bold"><?php echo $titDestGrupo; ?></h3>
    <?php echo $textDestGrupo; ?>
      </div>
    </div>
  </div>
</section>

