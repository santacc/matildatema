<?php
$imagenChicaEsp = get_field('imagenChicaEsp','options');
$iframeVideoEntradas = get_field('iframeVideoEntradas','options');
$urlEntradasEspecial = get_field('urlEntradasEspecial','options');
$imagenVentaEspecial = get_field('imagenVentaEspecial','options');
$imagenBotonVentaEspecial = get_field('imagenBotonVentaEspecial','option');

$paddingY = 0;

if( have_rows('headerEspecial') ):
  while( have_rows('headerEspecial') ):
    the_row();
    // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');
    $paddingY = get_sub_field('paddingY');

  endwhile;
endif;
$textoInferiorEnt = get_field('textoInferiorEnt','options');
$contHeaderEspecial = '';

    if($tipoFondoSec == 'color') {
      $contHeaderEspecial .= '<section id="contHeaderEspecial" class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
    } else if($tipoFondoSec == 'imagen') {
      $contHeaderEspecial .= '<section id="contHeaderEspecial" class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
    }
    $contHeaderEspecial .= '<div class="container"><div class="row justify-content-center">';
    $contHeaderEspecial .=  '<div class="col-12 col-md-6"><img src="'. $imagenVentaEspecial["url"] .'" width="90%"></div>
      <div class="col-12 col-md-6 mx-auto text-center px-5 contParaVideo">
        '.$iframeVideoEntradas.'<div class="introBoton">'.$textoInferiorEnt.' </div>
        <a href="'. $urlEntradasEspecial.'" target="_blank">
          <img src="'. $imagenBotonVentaEspecial["url"].'" class="imagenHeaderEspecial">
        </a>
      </div>';
    $contHeaderEspecial .= '</div></div><hr class="lineaSeparacion"></section>';

    echo $contHeaderEspecial;
?>

