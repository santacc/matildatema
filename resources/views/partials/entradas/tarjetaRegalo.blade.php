<?php
//tarjeta Regalo

$imagenTarjeta = get_field('imagenTarjeta','option');
$titTarjeta = get_field('titTarjeta','option');
$subTitTarjeta = get_field('subTitTarjeta','option');
$textoTarjeta = get_field('textoTarjeta','option');
$urlLinkTarjeta = get_field('urlLinkTarjeta','option');

$contTarjetaRegalo = '';

$paddingY = 0;
/*
if( have_rows('tarjetaRegalo') ):
  while( have_rows('tarjetaRegalo') ):
    the_row();
    // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');

    $paddingY = get_sub_field('paddingY');
  endwhile;
endif;
*/

$listadoEspecialesTarjeta = '';

$listadoEspecialesTarjeta .= '<ul class="listaSeguridad">';
if( have_rows('listadoEspecialesTerjeta','options') ):
  $contlistadoEspecialesTarjeta = '';
  while( have_rows('listadoEspecialesTerjeta','options') ):
    the_row();
    $itemListadoEspecialesTarjetas = get_sub_field('itemListadoEspecialesTarjetas');
    $contlistadoEspecialesTarjeta .= '<li>'.$itemListadoEspecialesTarjetas.'</li>';

  endwhile;

endif;
$listadoEspecialesTarjeta .= $contlistadoEspecialesTarjeta.'</ul>';

$contTarjetaRegalo .= '<section class="tipoVentas" style="background-color: black; color: white; padding: 2rem 0;"><div class="container pt-3 pb-3" >
                                <div class="row justify-content-center">
                                    <div class="col-10 p-0 col-md-5 p-md-5">
                                        <a href="'. $urlLinkTarjeta .'" target="_blank"><img src="'. $imagenTarjeta["url"].'" width="100%"></a>
                                    </div>
                                    <div class="col-12 p-0 col-md-6 p-md-5  contsegurida">
                                        <h4 class="tituloCompraSeguridad"> '.$titTarjeta.'</h4>
                                        <div class="subTitulo">'.$subTitTarjeta.'</div>
                                        <div class="tituloLista"> '.$textoTarjeta.'</div>
                                        <a href="'. $urlLinkTarjeta. '" target="_blank" class="btnComprarEspecial">COMPRAR TARJETA</a>
                                        '.$listadoEspecialesTarjeta.'
                                    </div>
                                </div>
                            </div>
                          </section>';
$contTarjetaRegalo .='</div></div></section>';

/*
$contenedorEspecial ='';
$mostrarDestacado = get_field('mostrarDestacado','option');
$titDestacadoEspecial = get_field('titDestacadoEspecial','option');
$subtitDestacadoEspecial = get_field('subtitDestacadoEspecial','option');
$preDestacadoEspecial = get_field('preDestacadoEspecial','option');
$listadoEspeciales='';
$listadoEspeciales .= '<ul class="listaSeguridad">';
if( have_rows('listadoEspeciales','option') ):
  while( have_rows('listadoEspeciales','option') ):
    the_row();
    $itemListadoEspeciales = get_sub_field('itemListadoEspeciales');
    $listadoEspeciales .= '<li>'.$itemListadoEspeciales.'</li>';
  endwhile;
endif;
$listadoEspeciales .= '</ul>';
$txtDestacadoEspecial = get_field('txtDestacadoEspecial','option');
$imagenEspecialDerecha = get_field('imagenEspecialDerecha','option');
$imagenDestacadoEspecial = get_field('imagenDestacadoEspecial','option');
$urlEspecial = get_field('urlEspecial','option');
if($tipoFondoSec == 'color') {
  $contenedorEspecial .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.'; padding: '.$paddingY.'px 0;">';
} else if($tipoFondoSec == 'imagen') {
  $contenedorEspecial .= '<section class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.'; padding: '.$paddingY.' 0;">';
}
$contenedorEspecial .= '<div class="container pt-3 pb-3" >
                                <div class="row justify-content-center">
                                    <div class="col-10 p-0 col-md-5 p-md-5">
                                        <a href="'. $urlEspecial.'" target="_blank"><img src="'. $imagenDestacadoEspecial["url"].'" width="100%"></a>
                                    </div>
                                    <div class="col-12 p-0 col-md-6 p-md-5  contsegurida">
                                        <h4 class="tituloCompraSeguridad"> '.$titDestacadoEspecial.'</h4>
                                        <div class="subTitulo">'.$subtitDestacadoEspecial.'</div>
                                        <div class="tituloLista"> '.$preDestacadoEspecial.'</div>
                                        <a href="'. $urlEspecial. '" target="_blank" class="btnComprarEspecial">COMPRAR TARJETA</a>
                                        '.$listadoEspeciales.'
                                    </div>
                                </div>
                            </div>
                          </section>';
*/

  echo $contTarjetaRegalo;

?>

