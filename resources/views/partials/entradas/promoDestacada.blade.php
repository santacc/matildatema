<?php
      $imagenPromocion = get_field('imagenPromocion','options');
      $textoBotonPromocion = get_field('textoBotonPromocion','options');
      $textoPromocion = get_field('textoPromocion','options');
      $urlLinkPromocion = get_field('urlLinkPromocion','options');

      $contPromoDestacada = '';
    if( have_rows('promocionEspecial') ):
      while( have_rows('promocionEspecial') ):
        the_row();
        // Get sub field values.
        $tipoFondoSec = get_sub_field('tipoFondoSup');
        $colorFondo = get_sub_field('fondoColorSup');
        $imageFondoSec = get_sub_field('imageFondoSup');
        $urlVideoSec = get_sub_field('urlVideoSup');
        $colorTextoSec = get_sub_field('colorTextoSup');
        $paddingY = get_sub_field('paddingY');
      endwhile;
    endif;


      if($tipoFondoSec == 'color') {
        $contPromoDestacada .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
      } else if($tipoFondoSec == 'imagen') {
        $contPromoDestacada .= '<section class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
      } else {
        $contPromoDestacada .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';">';
        /* $contenedorTipoVenta .= '<section class="py-2 my-2 conVideo"><div style="width: 100%; position:absolute; height: 100%; background-color: rgba(0,0,0,0.2)"></div>
           <div class="miPrueba"> <iframe class="videoDesk" src="'.$urlVideoSec.'?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';*/
      }
      $contPromoDestacada .= '<div class="container"><div class="row justify-content-center">';
      $contPromoDestacada .= '<div class="col-12 col-md-3 p-4"><a href="'.$urlLinkPromocion.'" target="_blank"><img src="'.$imagenPromocion["url"].'" width="100%"></a></div><div class="col-12 col-md-5 p-4" style="align-self: center">
        <div style="font-size: 3rem; color: #91D1F5; font-weight: bold; line-height: 1.3">VENTA  <span style="color: #2B3459">ANTICIPADA</span></div>
   '.$textoPromocion.'

      </div>';
    $contPromoDestacada .='</div></div></section>';

$contVentajas = '';
if( have_rows('ventajas','options') ):
  while( have_rows('ventajas','options') ):
    the_row();
    // Get sub field values.
    $imagenIcoVentajas = get_sub_field('imagenIcoVentajas');
    $textoVentajas = get_sub_field('textoVentajas');
    $contVentajas .='<div class="col-6 col-md-4 text-center">
        <img src="'.$imagenIcoVentajas["url"].'" class="imagenVentajaIcono">
        <p class="parrafVentajaTxt">'.$textoVentajas.'</p>
      </div>';
  endwhile;
endif;
$textoFinalVentajas = get_field('textoFinalVentajas','options');
$textoIntroVentajas = get_field('textoIntroVentajas','options');

?>

<section id="secVentajas" class="py-5" style="background-color: <?php echo $colorFondo; ?>; color: <?php echo $colorTextoSec ?>;  padding: <?php echo $paddingY; ?>px 0;">
  <h3 class="tituloVentajas"><?php echo $textoIntroVentajas;?></h3>
  <div class="container">
    <div class="row">
     <?php echo $contVentajas; ?>
    </div>
    <div class="row p-0 m-0">
      <div class="col-12 text-center p-0 m-0"><p class="textoInfVentajas"><?php echo $textoFinalVentajas;?></p></div>
    </div>
    <div class="row">
      <div class="col-12">

      </div>
    </div>
  </div>

</section>
