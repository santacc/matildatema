<?php
$textoIntro = get_field('entradasUno','options');
$textoDebajoBoton = get_field('entradasDos','options');
$imagenVenta = get_field('imagenVenta','options');
$urlEntradas = get_field('urlEntradas','options');

$mostrarDestacado = get_field('mostrarDestacado','option');
$titDestacadoEspecial = get_field('titDestacadoEspecial','option');
$subtitDestacadoEspecial = get_field('subtitDestacadoEspecial','option');
$preDestacadoEspecial = get_field('preDestacadoEspecial','option');
$listadoEspeciales='';
$listadoEspeciales .= '<ul class="listaSeguridad">';
if( have_rows('listadoEspeciales','option') ):
  while( have_rows('listadoEspeciales','option') ):
    the_row();
    $itemListadoEspeciales = get_sub_field('itemListadoEspeciales');
    $listadoEspeciales .= '<li>'.$itemListadoEspeciales.'</li>';
  endwhile;
endif;
$listadoEspeciales .= '</ul>';
$txtDestacadoEspecial = get_field('txtDestacadoEspecial','option');
$imagenEspecialDerecha = get_field('imagenEspecialDerecha','option');
$imagenDestacadoEspecial = get_field('imagenDestacadoEspecial','option');
$urlEspecial = get_field('urlEspecial','option');

$contenedorEspecial = '<section id="seccionEspecial">
                            <div class="container pt-3 pb-3" >
                                <div class="row justify-content-center">
                                    <div class="col-10 p-0 col-md-5 p-md-5">
                                        <a href="'. $urlEspecial.'" target="_blank"><img src="'. $imagenDestacadoEspecial["url"].'" width="100%"></a>
                                    </div>
                                    <div class="col-12 p-0 col-md-6 p-md-5  contsegurida">
                                        <h4 class="tituloCompraSeguridad"> '.$titDestacadoEspecial.'</h4>
                                        <div class="subTitulo">'.$subtitDestacadoEspecial.'</div>
                                        <div class="tituloLista"> '.$preDestacadoEspecial.'</div>
                                        <a href="'. $urlEspecial. '" target="_blank" class="btnComprarEspecial">COMPRAR TARJETA</a>
                                        '.$listadoEspeciales.'
                                    </div>
                                </div>
                            </div>
                          </section>';

if( have_rows('seccionSuperior') ):
  while( have_rows('seccionSuperior') ):
    the_row();

        // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');

    $paddingY = get_sub_field('paddingY');

 endwhile;
 endif;




$contenedorSup = '';

  if($tipoFondoSec == 'color') {
    $contenedorSup .= '<section id="secSuperiorEntradas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';">';
  } else if($tipoFondoSec == 'imagen') {
    $contenedorSup .= '<section id="secSuperiorEntradas" style="background-image: url('.$imageFondoSec["url"] .'); color: '.$colorTextoSec.';">';
  }
  $contenedorSup.='<div class="container"><div class="row justify-content-center"><div class="col-12 my-3">';
  $contenedorSup.='<h1 class="textoEncabezadoSuperior"> '.$textoIntro.' </h1></div>
                   <div class="row justify-content-center my-3">
                   <div class="col-10 col-md-9 mx-auto text-center">
                   <a href="'.$urlEntradas.'" target="_blank"><img src="'. $imagenVenta["url"].'" width="80%"></a></div>
                   </div>';
  $contenedorSup.='</div></div><hr class="lineaSeparacion"></section>';


if($mostrarDestacado == 'Si') {
  echo $contenedorEspecial;
}
  echo $contenedorSup;

  ?>
