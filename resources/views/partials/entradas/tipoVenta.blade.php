<?php
//Venta telefono taquilla
        $contenedorTipoVenta = '';


        if( have_rows('tipoVenta') ):
          while( have_rows('tipoVenta') ):
            the_row();
            // Get sub field values.
            $tipoFondoSec = get_sub_field('tipoFondoSup');
            $colorFondo = get_sub_field('fondoColorSup');
            $imageFondoSec = get_sub_field('imageFondoSup');
            $urlVideoSec = get_sub_field('urlVideoSup');
            $colorTextoSec = get_sub_field('colorTextoSup');
          endwhile;
        endif;

        if($tipoFondoSec == 'color') {
      $contenedorTipoVenta .= '<section id="tipoVenta" class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';">';
    } else if($tipoFondoSec == 'imagen') {
      $contenedorTipoVenta .= '<section id="tipoVenta" class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.'">';
    }

        $contenedorTipoVenta .= '<div class="container"><div class="row py-md-5 justify-content-center">';

  if( have_rows('contTipoVenta','options') ):
    while( have_rows('contTipoVenta','options') ): the_row();
      $titTipoVenta = get_sub_field('titTipoVenta');
      $textTipoVenta = get_sub_field('textTipoVenta');
      if($titTipoVenta == 'Teléfono') {
        $contenedorTipoVenta .= '<div class="col-12 col-md-3 text-center"><img src="/wp-content/uploads/2021/12/telefono_icono.png" alt="" width="34" height="34" style="margin-bottom: 10px"/><h3 class="tituloSeccion"> '.$titTipoVenta.'</h3>'.$textTipoVenta.'</div>';
      } else {
        $contenedorTipoVenta .= '<div class="col-12 col-md-3 text-center"><img src="/wp-content/uploads/2021/12/taquillas_icono.png" alt="" width="34" height="34" style="margin-bottom: 10px"/><h3 class="tituloSeccion"> '.$titTipoVenta.'</h3>'.$textTipoVenta.'</div>';
      }

    endwhile;
  endif;

$contenedorTipoVenta .='</div></div></section>';
echo $contenedorTipoVenta;
?>



