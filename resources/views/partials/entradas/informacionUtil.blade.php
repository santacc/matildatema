<?php
    //Información útil

$paddingY = 0;
if( have_rows('informacionUtil') ):
  while( have_rows('informacionUtil') ):
    the_row();

    // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');
    $paddingY = get_sub_field('paddingY');
  endwhile;
endif;

$contenedorInfoUtil ='';
$titSeccionUtil = get_field('titSeccionUtil','options');

if($tipoFondoSec == 'color') {
  $contenedorInfoUtil .= '<section id="informaUtil" class="infoUtil" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
} else if($tipoFondoSec == 'imagen') {
  $contenedorInfoUtil .= '<section id="informaUtil" class="infoUtil" style="background-image: url('.$imageFondoSec["url"] .'); color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
}
$contenedorInfoUtil .= '<h3 class="text-center mititulo">'. $titSeccionUtil .'</h3>';
$contenedorInfoUtil .= '<div class="container"><div class="row">';

    if( have_rows('contSeccionUtil','options') ):
      while( have_rows('contSeccionUtil','options') ): the_row();

              $titContUtil = get_sub_field('titContUtil');
              $textContUtil = get_sub_field('textContUtil');
              $contenedorInfoUtil .= '<div class="col-12 col-md-4 col-lg-4 p-4 mb-3 text-center px-5"><h4 class="tituloSeccionInt">'. $titContUtil .'</h4>'.  $textContUtil.'</div>';

              endwhile;
    endif;

    $contenedorInfoUtil .='</div></div></section>';

    echo $contenedorInfoUtil;

?>





