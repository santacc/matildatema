<?php

        $aclaracionPrecios = get_field('aclaracionPrecios','options');
        $tituloHorarios = get_field('tituloHorarios','options' );
        $textHorarios = get_field('textHorarios', 'options');
        $aclaracionHorarios = get_field('aclaracionHorarios', 'options');
        $planoTeatro = get_field('planoTeatro', 'options');
        $imgPlanoHorario = get_field('imgPlanoHorario', 'options');

        $urlComprarHorarios = get_field('urlComprarHorarios','options');

        $paddingY = 0;
        if( have_rows('preciosTeatro') ):
          while( have_rows('preciosTeatro') ):
              the_row();
              // Get sub field values.
              $tipoFondoSec = get_sub_field('tipoFondoSup');
              $colorFondo = get_sub_field('fondoColorSup');
              $imageFondoSec = get_sub_field('imageFondoSup');
              $urlVideoSec = get_sub_field('urlVideoSup');
              $colorTextoSec = get_sub_field('colorTextoSup');

            $paddingY = get_sub_field('paddingY');
            endwhile;
          endif;
        $contPreciosTabla = '<table style="width: 100%"><tr><td></td><td style="font-size: 1rem; text-align: center; font-weight: bold; color: #ff9d00; text-transform: uppercase">Miércoles <br />y jueves</td><td style="font-size: 1rem; text-align: center; font-weight: bold; color: #ff9d00; text-transform: uppercase">Viernes <br />y domingo </td><td style="font-size: 1rem; text-align: center; font-weight: bold; color: #ff9d00; text-transform: uppercase">Sábado</td></tr>';
        if( have_rows('contPrecio','options') ):
          while( have_rows('contPrecio','options') ): the_row();

            $tipoButaca = get_sub_field('tipoButaca');
            $precioButaca = get_sub_field('precioButaca');
            $precioButacaViDo = get_sub_field('precioButacaViDo');
            $precioButacaSa = get_sub_field('precioButacaSa');
            $contPreciosTabla.= '<tr style="border-bottom: 1px solid #fff"><td style="padding: 1%;">'.$tipoButaca.'</td><td style="padding: 1%; text-align: center"> '.$precioButaca.' </td><td style="padding: 1%; text-align: center">'.$precioButacaViDo.'</td><td style="padding: 1%; text-align: center">'.$precioButacaSa.'</td> </tr>';

          endwhile;
        endif;
        $contPreciosTabla .= '</table>';

        $contenedorPrecios = '';
        if($tipoFondoSec == 'color') {
          $contenedorPrecios .= '<section id="entradasPrecios" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
        } else if($tipoFondoSec == 'imagen') {
          $contenedorPrecios .= '<section id="entradasPrecios" style="background-image: url('.$imageFondoSec["url"] .'); color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
        } else {
        $contenedorPrecios .= '<section id="entradasPrecios" class="conVideo"><div style="width: 100%; position:absolute; height: 100%; background-color: rgba(0,0,0,0.2)"></div>
            <div class="miPrueba"> <iframe class="videoDesk" src="'.$urlVideoSec.'?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
        }

        $contenedorPrecios .='<div class="container">
                                            <div class="row">';
        $contenedorPrecios .='<div class="col-12 col-md-7 mb-5">'. $contPreciosTabla .'<div style="font-size: 0.8rem">'. $aclaracionPrecios .'</div>
                                                    <div style="margin-top: 5%;"><strong style="color: #ff9d00">'.$tituloHorarios.'</strong><br />
                                                                                        '. $textHorarios .'</div>';
        if($urlComprarHorarios != '') {
          $contenedorPrecios .= '<div class="zonaComprar"><a class="btn btnRecordatorio" href="'.$urlComprarHorarios.'" target="_blank" rel="noopener">COMPRAR</a></div>';
        }
        $contenedorPrecios .= '<div style="font-size: 0.8rem"> '. $aclaracionHorarios.'</div>';
        $contenedorPrecios .= '</div>
                                                <div class="col-12 col-md-5">
                                                    '. $planoTeatro.'
                                                </div>';
        $contenedorPrecios .='</div></div></section>';

        echo $contenedorPrecios;
?>
