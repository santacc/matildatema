<?php
$titInferiorUtil = get_field('titInferiorUtil','options');
$textInferiorUtil = get_field('textInferiorUtil','options');

$paddingY = 0;
if( have_rows('seccionAyuda') ):
  while( have_rows('seccionAyuda') ):
    the_row();
    // Get sub field values.
    $tipoFondoSec = get_sub_field('tipoFondoSup');
    $colorFondo = get_sub_field('fondoColorSup');
    $imageFondoSec = get_sub_field('imageFondoSup');
    $urlVideoSec = get_sub_field('urlVideoSup');
    $colorTextoSec = get_sub_field('colorTextoSup');

    $paddingY = get_sub_field('paddingY');
  endwhile;
endif;
$contseccionAyuda = '';

if($tipoFondoSec == 'color') {
  $contseccionAyuda  .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
} else if($tipoFondoSec == 'imagen') {
  $contseccionAyuda  .= '<section class="tipoVentas" style="background-image: url('.$imageFondoSec["url"].'); background-size: cover; background-position: bottom; color: '.$colorTextoSec.';  padding: '.$paddingY.'px 0;">';
} else {
  $contseccionAyuda  .= '<section class="tipoVentas" style="background-color: '. $colorFondo .'; color: '.$colorTextoSec.';">';
  /* $contenedorTipoVenta .= '<section class="py-2 my-2 conVideo"><div style="width: 100%; position:absolute; height: 100%; background-color: rgba(0,0,0,0.2)"></div>
     <div class="miPrueba"> <iframe class="videoDesk" src="'.$urlVideoSec.'?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';*/
}

$contseccionAyuda .= '<div class="container"><div class="row justify-content-center py-md-5">';
$contseccionAyuda .= '<div class="col-12 col-md-4 text-center"><h4>'. $titInferiorUtil .'</h4></div><div class="col-12 col-md-4">'. $textInferiorUtil.'</div>';
$contseccionAyuda .= '</div></div></section>';


echo $contseccionAyuda;
?>

