<?php
$micolorFondo = get_field('colorFondo' , 'options');
$micolorPrincipal = get_field('colorPrincipal' , 'options');
$miFontFamily = get_field('codigoGoogleFont', 'options');
$codigoTipoGeneral = get_field('codigoTipoGeneral', 'options');
$colorLink = get_field('colorLink', 'options');
?>

<?php echo $miFontFamily; ?>
<style>
  a {
    color: <?php echo $colorLink; ?>;
  }
  html, body {
    background-color: <?php echo $micolorFondo; ?>;
    color: <?php echo $micolorPrincipal; ?>;
  }
  header .navbar-nav a.nav-link {
    color: <?php echo $colorLink; ?>;
  }
</style>
