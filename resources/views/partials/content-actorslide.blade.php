<?php
  global $post;

  $fotoActor = get_field('fotoActor');
  $cvActor = get_field('cvActor');
  $personajePrincipal = get_field('personajePrincipal');
  $segundoPersonaje = get_field('segundoPersonaje');
  $terceroPersonaje = get_field('terceroPersonaje');
  $cuartoPersonaje = get_field('cuartoPersonaje');
  $quintoPersonaje = get_field('quintoPersonaje');





    $personajesPrincipales = '';
    $personajesPrincipales .= '<div class="actorGridExte" id="'. $post->ID.'"><div class="actorGrid">';
    $personajesPrincipales .= '<img src="'. $fotoActor["url"].'" width="100%">';
    $personajesPrincipales .= '<div class="titNomActor">'. get_the_title() .'</div>';
    $personajesPrincipales .= '<div class="contCurriculo" id="cont_'. $post->ID .'"><div class="mb-staff-arrow col2"><div class="mb-staff-arrow-outer"></div><div class="mb-staff-arrow-inner"></div></div><div class="container"><div class="row"><div class="col-3" style="background-image: url('.$fotoActor["url"].' ); background-size: cover; "></div><div class="col-8" style="position: relative; background-color: black"><div class="btnCerrar" data-cierre="'. $post->ID .'">X</div><div class="nombreActor">'. $cuartoPersonaje .'</div><div class="cvActor">'.$cvActor.'</div></div></div></div></div>';
    $personajesPrincipales .= '</div>';
$personajesPrincipales .= '<div class="fondoPersonajes">';
  if($personajePrincipal != '') {
    $personajesPrincipales .= '<div class="persoPrinActor">'. $personajePrincipal .'</div>';
  }

  if($segundoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoSegundoActor">'. $segundoPersonaje .'</div>';
  }

  if($terceroPersonaje != '') {
    $personajesPrincipales .= '<div class="persoTerceroActor">'. $terceroPersonaje .'</div>';
  }

  if($cuartoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoCuartoActor">'. $cuartoPersonaje .'</div>';
  }

  if($quintoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoQuintoActor">'. $quintoPersonaje .'</div>';
  }
    $personajesPrincipales .= '</div></div>';
   // echo $personajesPrincipales;

$destacadosHome = '';



$destacadosHome .= $personajesPrincipales;


// Do something...





echo $destacadosHome;
?>
<style>
  .mb-staff-arrow-inner {
    position: absolute;
    bottom: 0;
    left: 1px;
    width: 0;
    height: 0;
    z-index: 2;
    border-left: 14px solid transparent;
    border-right: 14px solid transparent;
    border-bottom: 14px solid #000000;
    transition: border-bottom-width .25s ease-in-out;
    -webkit-transition: border-bottom-width .25s ease-in-out;
  }
  .mb-staff-arrow-outer {
    position: absolute;
    bottom: 0;
    width: 0;
    height: 0;
    z-index: 1;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #969697;
    transition: border-bottom-width .25s ease-in-out;
    -webkit-transition: border-bottom-width .25s ease-in-out;
  }


    .leader-content .mb-staff-arrow, #leader-accordion li:nth-of-type(3n+1) .leader-content .mb-staff-arrow, #leader-accordion li:nth-of-type(3n+3) .leader-content .mb-staff-arrow {
      margin-top: -36px;
    }
</style>
