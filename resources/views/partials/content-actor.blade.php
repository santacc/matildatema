<?php
  global $post;


  $fotoActor = get_field('fotoActor');
  $cvActor = get_field('cvActor');
  $personajePrincipal = get_field('personajePrincipal');
  $segundoPersonaje = get_field('segundoPersonaje');
  $terceroPersonaje = get_field('terceroPersonaje');
  $cuartoPersonaje = get_field('cuartoPersonaje');
  $quintoPersonaje = get_field('quintoPersonaje');


    $personajesPrincipales = '';
    $personajesPrincipales .= '<div class="col-6 col-md-3 p-5 actorGridExte" id="'. $post->ID.'"><div class="actorGrid"><a href="'. $post->guid .'" class="linkActor"></a>';
    $personajesPrincipales .= '<img src="'. $fotoActor["url"].'" width="100%">';
    $personajesPrincipales .= '<div class="titNomActor">'. get_the_title() .'</div>';
    $personajesPrincipales .= '<div class="contCurriculo" id="cont_'. $post->ID .'"><div class="container"><div class="row"><div class="col-3" style="background-image: url('.$fotoActor["url"].' ); background-size: cover; "></div><div class="col-8" style="position: relative; background-color: black"><div class="btnCerrar" data-cierre="'. $post->ID .'">X</div><div class="nombreActor">'. $cuartoPersonaje .'</div><div class="cvActor">'.$cvActor.'</div></div></div></div></div>';
    $personajesPrincipales .= '</div>';
$personajesPrincipales .= '<div class="fondoPersonajes">';
  if($personajePrincipal != '') {
    $personajesPrincipales .= '<div class="persoPrinActor">'. $personajePrincipal .'</div>';
  }

  if($segundoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoSegundoActor">'. $segundoPersonaje .'</div>';
  }

  if($terceroPersonaje != '') {
    $personajesPrincipales .= '<div class="persoTerceroActor">'. $terceroPersonaje .'</div>';
  }

  if($cuartoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoCuartoActor">'. $cuartoPersonaje .'</div>';
  }

  if($quintoPersonaje != '') {
    $personajesPrincipales .= '<div class="persoQuintoActor">'. $quintoPersonaje .'</div>';
  }
    $personajesPrincipales .= '</div></div>';
    echo $personajesPrincipales;

?>
