
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--  <meta name="facebook-domain-verification" content="lncmwuc18adw6m5praq94d1cwj1ct8" /> -->
  <meta name="facebook-domain-verification" content="v8djzp6wkf9e5mvrnezd81qrwlilxc" />

  @php wp_head() @endphp
  @include('partials.style')
</head>

