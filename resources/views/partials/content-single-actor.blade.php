<div class="container my-5">
  <div class="row">
    <div class="col-12">
      <header>
        <h1 class="entry-title">{!! get_the_title() !!}</h1>
      </header>






    </div>
  </div>
  <div class="row">
    <div class="col-5">{{ next_post_link('%link', '<span>&laquo;</span> %title ') }}</div>
    <div class="col-2"></div>
    <div class="col-5 text-end">{{previous_post_link('%link', ' %title <span>&raquo;</span>') }}</div>
  </div>
</div>

<?php



  $fotoActor = get_field('fotoActor');
  $cvActor = get_field('cvActor');
  $personajePrincipal = get_field('personajePrincipal');
  $segundoPersonaje = get_field('segundoPersonaje');
  $terceroPersonaje = get_field('terceroPersonaje');
  $cuartoPersonaje = get_field('cuartoPersonaje');
  $quintoPersonaje = get_field('quintoPersonaje');

?>
    <div class="container my-5">
      <div class="row">

        <div class="col-12 col-md-3 p-0">
          <img src="<?php echo $fotoActor["url"]; ?>" width="100%" class="imgActor">
        </div>
        <div class="col-12 col-md-9">
              <?php if($personajePrincipal != ''){ ?>
              <strong>PERSONAJES</strong><br />
                <div class="nomPrin"> <?php echo $personajePrincipal; ?></div>
                <?php } ?>
                <?php if($segundoPersonaje != ''){ ?>
              <?php echo $segundoPersonaje; ?><br />
                <?php } ?>
                <?php if($terceroPersonaje != ''){ ?>
              <?php echo $terceroPersonaje; ?><br />
                <?php } ?>
                <?php if($cuartoPersonaje != ''){ ?>
              <?php echo $cuartoPersonaje; ?><br />
                <?php } ?>
                <?php if($quintoPersonaje != ''){ ?>
              <?php echo $quintoPersonaje; ?><br />
                <?php } ?>


            <div class="cvActor mt-4"><?php echo $cvActor; ?></div>
        </div>
      </div>
    </div>
