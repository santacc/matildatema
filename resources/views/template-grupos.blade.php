{{--
  Template Name: template Grupos
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.page-header')
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 col-lg-6">
        @include('partials.content-page')
      </div>
    </div>
  </div>


  @endwhile
@endsection
