// import external dependencies

import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import templateElenco from './routes/template-elenco';
import templatePrograma from './routes/template-programa';
import templateElencoslide from './routes/template-elencoslide';
import templateElencolista from './routes/template-elencolista';
import templateOpiniones from './routes/template-opiniones';
import templateEntradas from './routes/template-entradas';
import templateEquipocreativo from './routes/template-equipocreativo';


/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,

  templateElenco,

  templateElencoslide,

  templateElencolista,

  templatePrograma,

  templateOpiniones,

  templateEntradas,

  templateEquipocreativo,


});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
