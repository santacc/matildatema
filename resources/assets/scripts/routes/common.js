export default {
  init() {
    // JavaScript to be fired on all pages
    let contador = 0;
    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      if(contador == 0 ) {
        $('.menuMovil').animate({
          'display': 'block',
          'left': '0',
        }, 500);
        contador = 1;
      } else {
        $('.menuMovil').animate({
          'display': 'block',
          'left': '-100vw',
        }, 500);
        contador = 0;
      }
    });

   /* $('.menuMovil .nav-link').on('click', function() {
      $('.hamburger').removeClass('is-active');
      $('.menuMovil').animate({
        'display': 'block',
        'left': '-100vw',
      }, 500);
      contador = 0;
    })*/

    var altura = $('.banner').offset().top;

    $(window).on('scroll', function(){
      if ( $(window).scrollTop() > altura ){
        $('.banner').addClass('menu-fixed');
      } else {
        $('.banner').removeClass('menu-fixed');
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
