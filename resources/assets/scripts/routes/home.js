import anime from 'animejs';
//import Letterize from 'letterizejs';
/* import { gsap } from 'gsap' ;
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { CSSPlugin } from 'gsap/CSSPlugin';
import { TextPlugin } from 'gsap/TextPlugin'; */


export default {
  init() {



    $('#quitar-mute').click( function (){
      if( $('video').prop('muted', true) )
      {
        $('video').prop('muted', false);
        $('#quitar-mute').css('display','none');
        $('#mute-video').css('display','block');

      }
    });

    $('#mute-video').click( function (){
      $('video').prop('muted', true);
      $('#quitar-mute').css('display','block');
      $('#mute-video').css('display','none');
    });


/*
    gsap.registerPlugin(ScrollTrigger, CSSPlugin, TextPlugin);




    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.recordEntradas',
        start: 'center bottom',
        end: 'center top',
        scrub: true,
        markers: true,
      },
    });
    tl.to('.btn', {rotation: 45, duration: 1})
    tl.to('.btn', {rotation: 0,  duration: 2})
    tl.to('.imgSinopsis', {scale: 0.5,opacity: 0.2, duration: 2})
    tl.to('.imgSinopsis', {scale: 1, opacity: 1, duration: 3, delay: 4})
    tl.to('.imgSinopsis', {scale: 0.5, opacity: 0.2, duration: 5})

    //gsap.from('.sinopsis p', {duration: 3, text: ''})

*/


/*
    let aparecerScroll = gsap.to(
      '.imgSinopsis',
      {
        scrollTrigger: '.sinopsis', // start the animation when ".box" enters the viewport (once)
        x: -500,
    });

    aparecerScroll; */

   let muevoTexto = anime({
      targets: 'path, polygon',
      strokeDashoffset: [anime.setDashoffset, 0],
      easing: 'easeInOutSine',
      duration: 1500,
      delay: function(el, i) { return i * 100 },
      //direction: 'alternate',
     // loop: true,
      complete: function() {
        $('svg .st0').css('fill', '#fff');
        $('svg .st1').css('fill','#91D1F5');
      },
    });


    muevoTexto.play();

 /*  let moverboton =  anime({
      targets: '.btn',
      scale: 1,
      rotate: 360,
      loop: true,
      direction: 'alternate',
     easing: 'easeInOutSine',
      duration: 2000,
      delay: 1000,
    });
    moverboton.play(); */

    let moverLibroUno =  anime({
      targets: '#libro1',
      keyframes: [
        {translateY: -10},
        {translateX: 20},
        {translateY: -10},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroUno.play();
    let moverLibroDos =  anime({
      targets: '#libro2',
      keyframes: [
        {translateY: 5},
        {translateX: -10},
        {translateY: 3},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroDos.play();
    let moverLibroTres =  anime({
      targets: '#libro3',
      keyframes: [
        {translateY: -5},
        {translateX: 4},
        {translateY: -3},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroTres.play();
    let moverLibroCuatro =  anime({
      targets: '#libro4',
      keyframes: [
        {translateY: 10},
        {translateX: -10},
        {translateY: 3},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroCuatro.play();
    let moverLibroCinco =  anime({
      targets: '#libro5',
      keyframes: [
        {translateY: -5},
        {translateX: 9},
        {translateY: -3},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroCinco.play();
    let moverLibroSeis =  anime({
      targets: '#libro6',
      keyframes: [
        {translateY: -5},
        {translateX: -4},
        {translateY: 30},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroSeis.play();
    let moverLibroSiete =  anime({
      targets: '#libro7',
      keyframes: [
        {translateY: -5},
        {translateX: 14},
        {translateY: -23},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroSiete.play();
    let moverLibroOcho =  anime({
      targets: '#libro8',
      keyframes: [
        {translateY: -5},
        {translateX: -10},
        {translateY: 3},
        {translateX: 0},
        {translateY: 0},
      ],
      duration: 5000,
      direction: 'alternate',
      easing: 'easeInOutSine',
      delay: 50,
      loop: true,
    });
    moverLibroOcho.play();
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
