
import Glide from '@glidejs/glide';
import { Images } from '@glidejs/glide/dist/glide.modular.esm'

export default {
  init() {
    // JavaScript to be fired on the about us page
    console.log('arranca el elenco slide');

    let myVar;
    let myVarDos;
    $('.actorGridExte').on('click',function(){
      myVar = $(this).attr('id');
      $('.contCurriculo').fadeOut('fast');
      $('#cont_' + myVar).css('display','flex');

    });

    $('.btnCerrar').on('click',function(){
      myVarDos = $(this).data('cierre');
      $('#cont_' + myVarDos).fadeOut('fast');
    });

    $('.glide__arrow').on('click',function(){
      $('.contCurriculo').fadeOut('fast');
    });


    let posicion = 0;

    let glide = new Glide('.glideDestacados', {
      type: 'slider',
      perView: 5,
      startAt: posicion,
      focusAt: 2,
      breakpoints: {
        500: {
          perView: 1,
          startAt: 0,
          focusAt: 0,
        },
        600: {
          perView: 2,
          startAt: 0,
          focusAt: 0,
        },
        990: {
          perView: 3,
          startAt: 0,
          focusAt: 1,
        },
        1200: {
          perView: 5,
        },
        1600: {
          perView: 5,
        },
      },
    });



    glide.on(['mount.after'], function() {
      $('.glideDestacados').css('opacity', 1);
    })
    glide.mount({ Images });

    glide.on(['move.after'], function() {
      $('.contCurriculo').css('display','none');
      let miVarPrueba = $('.glide__slide--active .actorGridExte').attr('id');
      $('#cont_' + miVarPrueba).css('display','flex');

    })


    $('.destacadoisGrid').on('click',function(){
      $('.contCurriculo').css('display','none');
      myVarDos = $(this).data('position');
      glide.update({
        startAt: myVarDos,
      });

    });

    /* let miArrayClases;

    miArrayClases = document.getElementsByClassName('glide__slide');
    console.log(miArrayClases); */

  },
};

