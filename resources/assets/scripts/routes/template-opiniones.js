
import Glide from '@glidejs/glide';

export default {
  init() {
    // JavaScript to be fired on the about us page
    console.log('arranca el elenco slide');

    let glide = new Glide('.glide', {
      type: 'carousel',
      perView: 4,
      breakpoints: {
        400: {
          perView: 1,
        },
        600: {
          perView: 2,
        },
        990: {
          perView: 3,
        },
        1200: {
          perView: 3,
        },
        1600: {
          perView: 4,
        },
      },
    });
    glide.mount();

    /* let miArrayClases;

    miArrayClases = document.getElementsByClassName('glide__slide');
    console.log(miArrayClases); */

  },
};

