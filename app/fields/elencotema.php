<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$elencotema = new FieldsBuilder('elencotema');

$elencotema
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-elenco.blade.php');

$elencotema
    ->addRepeater('personaje', [
        'label' => 'Personaje',
        'instructions' => '',
        'layout' => 'row',
    ])
        ->addText('nombrePersonaje', [
            'label' => 'Nombre del personaje',
        ])
        ->addSelect('tipoPersonaje', [
            'label' => 'Select Field',
            'choices' => [
                'principal' => 'Principal',
                'secundarios' => 'Secundario',
                'elenco'=> 'Elenco',
            ],
            'ui' => 1,
        ])
        ->addNumber('numeroActores', [
            'label' => 'Número de actores que lo interpretan',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => '1',
            'max' => '4',
            'step' => '1',
        ])
            ->addPostObject('actores', [
                'label' => 'Actores que lo interpretan',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => ['actor'],
                'taxonomy' => [],
                'allow_null' => 0,
                'multiple' => 1,
                'return_format' => 'object',
                'ui' => 1,
            ])


    ->endRepeater()

;
return $elencotema;
