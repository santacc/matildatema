<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$teatro = new FieldsBuilder('teatro');

$teatro
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-teatro.blade.php');

$teatro
    ->addTab('Horarios y dirección', ['placement' => 'top'])
        ->addText('titDesatcadoUno', [
            'label' => 'Titulo destacado uno',
        ])
        ->addColorPicker('fondoSeccionUno', [
            'label' => 'Selccionar color de fondo de la primera seccion',
        ])
        ->addColorPicker('colorTxtSeccionUno', [
            'label' => 'Selccionar color del texto de la primera seccion',
        ])
        ->addWysiwyg('desDestacadoUno', [
            'label' => 'Contenido destacdo uno',
        ])
        ->addText('botonMapa', [
            'label' => 'Texto para el boton de mapa',
        ])
        ->addWysiwyg('horarioTeatro', [
            'label' => 'Horario del teatro',
        ])
        ->addImage('imagenTeatroSeguro', [
            'label' => 'Imagen Teatro seguro',
        ])
    ->addUrl('linkTeatroSeguro', [
        'label' => 'Url tetaro seguro',
    ])
    ->addTab('Como llegar y mapa', ['placement' => 'top'])
        ->addColorPicker('fondoSeccionDos', [
            'label' => 'Selccionar color de fondo de la segunda seccion',
        ])
        ->addColorPicker('colorTxtSeccionDos', [
            'label' => 'Selccionar color del texto de la segunda seccion',
        ])
        ->addText('iframeMapa', [
            'label' => 'Codigo del iframe',
        ])
        ->addText('titComoLlegar', [
            'label' => 'Titulo como llegar',
        ])
        ->addRepeater('mediosTransporte', [
            'label' => 'Medios transporte para llegar',
        ])
            ->addText('icoTransporte', [
                'label' => 'Icono medio transporte',
            ])
            ->addText('titMedioTransporte', [
                'label' => 'Titulo del medio de transporte',
            ])
            ->addWysiwyg('descMedioTransporte', [
                'label' => 'Descripcion medio de transporte',
            ])

        ->endRepeater()
    ->addTab('Información teatro', ['placement' => 'top'])
        ->addColorPicker('fondoSeccionTres', [
            'label' => 'Selccionar color de fondo de la tercera seccion',
        ])
        ->addColorPicker('colorTxtSeccionTno', [
            'label' => 'Selccionar color del texto de la tercera seccion',
        ])
        ->addText('titInfoTeatro', [
            'label' => 'Titulo info teatro',
        ])
        ->addWysiwyg('infoTeatro', [
            'label' => 'Informaciond el teatro',
        ])





;

return $teatro;

