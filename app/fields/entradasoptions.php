<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$entradasoptions = new FieldsBuilder('entradas_options');

$entradasoptions
    ->setLocation('options_page', '==', 'acf-options-entradas');

$entradasoptions
    ->addTab('Seccion superior Especial', ['placement' => 'left'])
        ->addImage('imagenVentaEspecial', [
            'label' => 'Imagen la nueva seccion de la chica',
        ])
        ->addImage('imagenBotonVentaEspecial', [
            'label' => 'Imagen para el boton de comprar entradas',
        ])
        ->addUrl('urlEntradasEspecial', [
            'label' => 'URL venta Entradas',
        ])
        ->addTextarea('iframeVideoEntradas', [
            'label' => 'Iframe para el video de la home',
        ])
        ->addTrueFalse('ocultarMenu', [
                    'label' => 'Activada / Desactivada el menu superior',
                    'instructions' => 'Oculta el menu superior',
                    'required' => 0,
                    'conditional_logic' => [],
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 1,
                    'ui_on_text' => 'Activado',
                    'ui_off_text' => 'Desactivado',
                ])
        ->addText('textoInferiorEnt', [
            'label' => 'Texto para el boton de comprar',
        ])
    ->addTab('Seccion superior Generica', ['placement' => 'left'])
            ->addText('entradasUno', [
                'label' => 'Texto para el boton de comprar entradas de la parte superior',
            ])
            ->addText('entradasDos', [
                'label' => 'Texto para debajo del boton',
            ])
            ->addImage('imagenVenta', [
                'label' => 'Imagen para la venta de entradas',
            ])
            ->addImage('imagenChica', [
                'label' => 'Imagen la nueva seccion de la chica',
            ])
            ->addUrl('urlEntradas', [
                'label' => 'URL venta Entradas',
            ])
    ->addTab('Promocion', ['placement' => 'left'])
        ->addImage('imagenPromocion', [
            'label' => 'Image Field',
        ])
        ->addWysiwyg('textoPromocion', [
            'label' => 'Texto para la Promocion',
        ])
        ->addText('textoBotonPromocion', [
        'label' => 'Ttitulo inferior seccion Información útil ',
    ])
        ->addUrl('urlLinkPromocion', [
        'label' => 'URL Compra tarjeta',
    ])
        ->addText('textoIntroVentajas', [
            'label' => 'Texto parte introducción ventajas ',
        ])
        ->addRepeater('ventajas', [
            'label' => 'Columnas Ventajas',

        ])
            ->addImage('imagenIcoVentajas', [
            'label' => 'Image Field',
        ])
            ->addWysiwyg('textoVentajas', [
        'label' => 'Texto para la Promocion',
    ])
        ->endRepeater()
        ->addText('textoFinalVentajas', [
            'label' => 'Texto parte inferior ventajas ',
        ])
    ->addTab('Seccion Tipo Ventas', ['placement' => 'left'])
        ->addRepeater('contTipoVenta', [
            'label' => 'Contenedores Tipo de ventas',
            'layout' => 'row',
        ])
            ->addText('titTipoVenta', [
                'label' => 'Titulo para el apartado venta telefono',
            ])
            ->addWysiwyg('textTipoVenta', [
            'label' => 'Texto para el apartado venta telefono',
        ])
        ->endRepeater()
    ->addTab('Seccion Precio / Teatro / Horarios', ['placement' => 'left'])
        ->addRepeater('contPrecio', [
            'label' => 'Filas de la tabal de precios',
            'layout' => 'row',
        ])
            ->addText('tipoButaca', [
                'label' => 'Texto para el tipo de butaca',
            ])
            ->addText('precioButaca', [
                'label' => 'Texto para el precio de martes a viernes tardes ',
            ])
            ->addText('precioButacaViDo', [
                'label' => 'Texto para el precio viernes noche y domingo',
            ])
            ->addText('precioButacaSa', [
                'label' => 'Texto para el precio sabado',
            ])
        ->endRepeater()
        ->addText('aclaracionPrecios', [
            'label' => 'Texto para la acalaración debajo de la tabla de precios',
        ])
        ->addText('tituloHorarios', [
            'label' => 'Texto para el titulo de los horarios',
        ])
        ->addWysiwyg('textHorarios', [
            'label' => 'Texto para los horarios',
        ])
        ->addText('aclaracionHorarios', [
            'label' => 'Texto para la acalaración debajo del texto de horarios',
        ])
        ->addUrl('urlComprarHorarios', [
            'label' => 'URL Compra entradas',
        ])
        ->addTextarea('planoTeatro', [
            'label' => 'SVG Plano tetro',
        ])
        ->addImage('imgPlanoHorario', [
        'label' => 'Imagen la plano del tetaro',
    ])
    ->addTab('Seccion Destacados Promociones', ['placement' => 'left'])
        ->addRepeater('contDestPromociones', [
            'label' => 'Contenedores destacados Promociones',
            'layout' => 'row',
        ])
            ->addText('titDestPromocion', [
                'label' => 'Titulo para el apartado Butaca Oro',
            ])
            ->addWysiwyg('textDestPromocion', [
                'label' => 'Texto para el apartado Butaca Oro',
            ])
            ->addImage('fondoDestPromocion', [
            'label' => 'Imagen para fondo butaca oro',
        ])
        ->endRepeater()
        ->addText('titDestGrupo', [
            'label' => 'Titulo para el apartado Grupos',
        ])
        ->addWysiwyg('textDestGrupo', [
            'label' => 'Texto para el apartado Grupo',
        ])
        ->addImage('fondoDestGrupo', [
        'label' => 'Imagen para fondo Grupos',
    ])
    ->addTab('Seccion Informacion Util', ['placement' => 'left'])
        ->addText('titSeccionUtil', [
        'label' => 'Titulo seccion Información útil ',
    ])
        ->addRepeater('contSeccionUtil', [
                'label' => 'Contenedores seccion Util',
                'layout' => 'row',
            ])
            ->addText('titContUtil', [
                'label' => 'Titulo para Teatro Seguro',
            ])
            ->addTextarea('textContUtil', [
                'label' => 'Texto para Teatro Seguro',
            ])
        ->endRepeater()
    ->addTab('Seccion Tarjeta Regalo', ['placement' => 'left'])
        ->addImage('imagenTarjeta', [
            'label' => 'Image Field',
        ])
        ->addText('titTarjeta', [
            'label' => 'Titulo superior para la tarjeta ',
        ])
        ->addText('subTitTarjeta', [
            'label' => 'Subtitulo para la tarjeta',
        ])
        ->addText('textoTarjeta', [
            'label' => 'Texto encima de la imagen',
        ])
        ->addUrl('urlLinkTarjeta', [
         'label' => 'URL Compra tarjeta',
        ])
        ->addRepeater('listadoEspecialesTerjeta', [
        'label' => 'Items para listado especiales',
        ])
            ->addText('itemListadoEspecialesTarjetas', [
            'label' => 'Item para el listado de los especiales',
            ])
        ->endRepeater();


return $entradasoptions;
