<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$opinion = new FieldsBuilder('campos_opinion');

$opinion
    ->setLocation('post_type', '==', 'opinion');

$opinion

    ->addTextarea('descOpinion', [
        'label' => 'Descripción de la opinión',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'maxlength' => '400',
    ])
    ->addText('nombreOpinion', [
        'label' => 'Nombre de la persona que da opinión',
    ])
    ->addDatePicker('fechaOpinion', [
        'label' => 'Fecha de la opinion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'display_format' => 'd/m/Y',
        'return_format' => 'd M Y',
        'first_day' => 1,
    ])
    ->addText('txtMedioOpinion', [
        'label' => 'Texto intro que da el medio',
    ])
    ->addText('nomMedioOpinion', [
        'label' => 'Nombre del medio que da opinión',
    ])
;
return $opinion;

