<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$faqtema = new FieldsBuilder('faqtema');

$faqtema
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-FAQ.blade.php');

$faqtema
    ->addTab('Preguntas Genéricas', ['placement' => 'left'])
        ->addText('tituloFAQGenericas', [
            'label' => 'Titulo del bloque FAQ Genericas',
        ])
        ->addRepeater('faqGenericas', [
            'label' => 'Preguntas frecuentes genéricas',
        ])
            ->addText('preguntaGenerica', [
                'label' => 'Pregunta para la FAQ generica',
            ])
            ->addWysiwyg('respuestaGenerica', [
                'label' => 'Respuesta para la FAQ generica',
            ])
        ->endRepeater()
    ->addTab('Preguntas sobre Teatro', ['placement' => 'left'])
        ->addText('tituloFAQTeatro', [
            'label' => 'Titulo del bloque FAQ sobre el teatro',
        ])
        ->addRepeater('faqSobreTeatro', [
            'label' => 'Preguntas frecuentes sobre el teatro',
        ])
            ->addText('preguntaSobreTeatro', [
                'label' => 'Pregunta para la FAQ teatro',
            ])
            ->addWysiwyg('respuestaSobreTeatro', [
                'label' => 'Respuesta para la FAQ generica',
            ])
        ->endRepeater()

;

return $faqtema;
