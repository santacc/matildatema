<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$estiloseccionhome = new FieldsBuilder('estilo-seccion-home');

$estiloseccionhome
    ->addTrueFalse('activeSeccion', [
        'label' => 'Activada / Desactivada la sección',
        'instructions' => 'Activar o desactivar la seccion',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Activado',
        'ui_off_text' => 'Desactivado',
    ])
    ->addTrueFalse('confSeccionesHome', [
        'label' => 'Activada / Desactivada las opciones de configuracion',
        'instructions' => 'Activar o desactivar las opciones de la configuracion de la seccion',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'activeSeccion',
            'operator' => '==',
            'value' => '1',
            ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Activado',
        'ui_off_text' => 'Desactivado',
    ])
    ->addSelect('tipoFondo', [
        'label' => 'Seleccionar el tipo de fondo de la sección',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'confSeccionesHome',
            'operator' => '==',
            'value' => '1',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            'imagen' => 'Imagen',
            'color'=> 'Color',
        ],
        'default_value' => [],
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 1,
        'ajax' => 0,
        'return_format' => 'value',
        'placeholder' => '',
    ])
    ->addColorPicker('fondoColor', [
        'label' => 'Seleccionar el color de fondo de la sección',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'tipoFondo',
            'operator' => '==',
            'value' => 'color',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
    ])
    ->addImage('imageFondo', [
        'label' => 'Imagen para el fondo de la seccion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'tipoFondo',
            'operator' => '==',
            'value' => 'imagen',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addText('urlVideo', [
        'label' => 'URL Vimeo del video',
        'conditional_logic' => [
            'field' => 'tipoFondo',
            'operator' => '==',
            'value' => 'video',
        ],
    ])
    ->addNumber('paddingY', [
        'label' => 'Padding en la parte superior e inferior',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'confSeccionesHome',
            'operator' => '==',
            'value' => '1',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '0',
        'max' => '50',
        'step' => '5',
    ])
    ->addColorPicker('colorTexto', [
        'label' => 'Seleccionar el color para la seccion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'confSeccionesHome',
            'operator' => '==',
            'value' => '1',
        ],
    ]);
return $estiloseccionhome;
