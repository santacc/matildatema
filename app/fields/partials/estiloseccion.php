<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$estiloseccion = new FieldsBuilder('estilo-seccion');

$estiloseccion
    ->addTrueFalse('verSeccionSup', [
        'label' => 'Activada / Desactivada la sección superior',
        'instructions' => 'Activar o desactivar la opción de la seccion superior',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Activado',
        'ui_off_text' => 'Desactivado',
    ])
    ->addSelect('tipoFondoSup', [
        'label' => 'Seleccionar el tipo de fondo de la sección',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'verSeccionSup',
            'operator' => '==',
            'value' => '1',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            'imagen' => 'Imagen',
            'color'=> 'Color',
        ],
        'default_value' => [],
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 1,
        'ajax' => 0,
        'return_format' => 'value',
        'placeholder' => '',
    ])
    ->addColorPicker('fondoColorSup', [
        'label' => 'Seleccionar el color de fondo de la sección',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'tipoFondoSup',
            'operator' => '==',
            'value' => 'color',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
    ])
    ->addImage('imageFondoSup', [
        'label' => 'Imagen para el fondo de la seccion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'tipoFondoSup',
            'operator' => '==',
            'value' => 'imagen',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addText('urlVideoSup', [
        'label' => 'URL Vimeo del video',
        'conditional_logic' => [
            'field' => 'tipoFondoSup',
            'operator' => '==',
            'value' => 'video',
        ],
    ])
    ->addNumber('paddingY', [
        'label' => 'Padding en la parte superior e inferior',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'verSeccionSup',
            'operator' => '==',
            'value' => '1',
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '0',
        'max' => '50',
        'step' => '5',
    ])
    ->addColorPicker('colorTextoSup', [
        'label' => 'Seleccionar el color para la seccion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'verSeccionSup',
            'operator' => '==',
            'value' => '1',
        ],
    ]);
return $estiloseccion;
