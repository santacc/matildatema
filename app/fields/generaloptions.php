<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$generaloptions = new FieldsBuilder('general_options');

$generaloptions
    ->setLocation('options_page', '==', 'acf-options-generales');

$generaloptions
    ->addTab('Configuarcion General', ['placement' => 'left'])
        ->addImage('logoHeader', [
            'label' => 'Logo para el header',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
        ])
        ->addImage('btnEntradas', [
        'label' => 'boton entradas del menu',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
            'return_format' => 'array',
    ])
    ->addTab('Colores', ['placement' => 'left'])
        ->addColorPicker('colorFondo', [
            'label' => 'Color para el fondo de la pagina',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
        ])
        ->addColorPicker('colorPrincipal', [
            'label' => 'Color principal para el tema',
            'instructions' => '',

        ])
        ->addColorPicker('colorLink', [
        'label' => 'Color para los links',
        'instructions' => '',
    ])
    ->addTab('Tipografia', ['placement' => 'left'])
        ->addTextarea('codigoGoogleFont', [
            'label' => 'Codigo Tipografia de Google Font>',
            'instructions' => 'Pegar codigo de google font <a href="https://fonts.google.com/" target="_blank"> Google font </a>',
        ])
        ->addText('codigoTipoGeneral', [
            'label' => 'Codigo tipo general',
            'instructions' => '',
        ])
    ->addTab('Configuracion del footer', ['placement' => 'left'])
        ->addImage('fondoFooter', [
        'label' => 'Imagen imagen de fondo para el footer',
    ]);


return $generaloptions;
