<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$actor = new FieldsBuilder('campos_actor');

$actor
    ->setLocation('post_type', '==', 'actor');

$actor


    ->addImage('fotoActor', [
        'label' => 'Foto del actor',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '250kb',
        'mime_types' => 'jpg',
    ])
    ->addWysiwyg('cvActor', [
        'label' => 'Curriculum Actor',
    ])
    ->addText('personajePrincipal', [
        'label' => 'Personaje principal',
    ])
    ->addText('segundoPersonaje', [
        'label' => 'Segundo personaje que interpreta',
    ])
    ->addText('terceroPersonaje', [
        'label' => 'Tercero personaje que interpreta',
    ])
    ->addText('cuartoPersonaje', [
        'label' => 'cuarto personaje que interpreta',
    ])
    ->addText('quintoPersonaje', [
        'label' => 'Quinto personaje que interpreta',
    ])
;

return $actor;

