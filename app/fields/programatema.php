<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$programatema = new FieldsBuilder('programatema');


$programatema
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-programa.blade.php');


$programatema
    ->addImage('fondoMovil', [
        'label' => 'Imagen para el fondo movil',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],

    ])
    ->addImage('imagenDesk', [
        'label' => 'Imagen para la imagen lateral',
        'instructions' => 'Imagen que se usará en desktop',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],

    ])

    ->addRepeater('botonesLink', [
        'label' => 'Botones',
    ])
        ->addText('textBoton', [
            'label' => 'Text Field',
        ])
        ->addUrl('urlBoton', [
            'label' => 'Enlace para el boton',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
        ])

    ->endRepeater()
;
return $programatema;



