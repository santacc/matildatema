<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$entradastema = new FieldsBuilder('entradastema');

$entradastema
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-entradas.blade.php');

$entradastema
    ->addTab('Seccion superior', ['placement' => 'left'])
        ->addGroup('seccionSuperior', [
            'label' => 'Seccion Superior',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Header especial entradas', ['placement' => 'left'])
        ->addGroup('headerEspecial', [
            'label' => 'Header Especial',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Seccion Promoción', ['placement' => 'left'])
        ->addGroup('promocionEspecial', [
            'label' => 'Promocion Especial',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Seccion Tipo Ventas', ['placement' => 'left'])
            ->addGroup('tipoVenta', [
                'label' => 'Seccion precios y horarios',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'layout' => 'Row',
                'sub_fields' => [],
            ])
                ->addFields(get_field_partial('partials.estiloseccion'))
            ->endGroup()
    ->addTab('Seccion Destacados Promociones', ['placement' => 'left'])
            ->addGroup('destacadosPromociones', [
                'label' => 'Seccion Destacados Promciones',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'layout' => 'Row',
                'sub_fields' => [],
            ])
                ->addFields(get_field_partial('partials.estiloseccion'))
            ->endGroup()
    ->addTab('Seccion Precios', ['placement' => 'left'])
        ->addGroup('preciosTeatro', [
            'label' => 'Seccion precios y horarios',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Seccion Informacion Util', ['placement' => 'left'])
        ->addGroup('informacionUtil', [
            'label' => 'Informacion util',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Seccion Tarjeta Regalo', ['placement' => 'left'])
        ->addGroup('tarjetaRegalo', [
            'label' => 'Seccion Tarjeta Regalo',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup()
    ->addTab('Seccion Ayuda', ['placement' => 'left'])
        ->addGroup('seccionAyuda', [
            'label' => 'Seccion Ayuda',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccion'))
        ->endGroup();

return $entradastema;

