<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$equipocreativo = new FieldsBuilder('equipocreativo');

$equipocreativo
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-equipocreativo.blade.php');

$equipocreativo
    ->addTab('Equipo Creativo', ['placement' => 'left'])
    ->addImage('fondoSeccion', [
        'label' => 'Fondo para la secciond e equipo creativo',
    ])
    ->addImage('logoSecciom', [
        'label' => 'Logo para la seccion de equipo creativo',
    ])
    ->addText('labelProduccion', [
        'label' => 'Produccion equipo creativo',
    ])
    ->addText('nombreProduccion', [
        'label' => 'Nombre productores',
    ])
    ->addText('labelDireccion', [
        'label' => 'Direccion equipo creativo',
    ])
    ->addText('nombreDireccion', [
        'label' => 'Nombre Director',
    ])
    ->addRepeater('EquipoCreativo', [
        'label' => 'Equipo creativo',
        'conditional_logic' => [
            'field' => 'activeSeccion',
            'operator' => '==',
            'value' => '1',
        ],
    ])
    ->addText('puestoEquipo', [
        'label' => 'Puesto en el equipo creativo',
    ])
    ->addText('nombreEquipo', [
        'label' => 'Nombre de la o las personas',
    ])
    ->endRepeater()
    ->addText('labelProductoresEjecutivos', [
        'label' => 'Productores ejecutivos',
    ])
    ->addText('nombreProductoresEjecutivos', [
        'label' => 'Nombre productores',
    ])
    ->addText('labelProductores', [
        'label' => 'Etiqueta productores',
    ])
    ->addText('nombreProductores', [
        'label' => 'Nombre Productores',
    ])

;

return $equipocreativo;

