<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


    acf_add_options_page([
        'page_title' => get_bloginfo('name') . ' theme options',
        'menu_title' => 'Opciones',
        'menu_slug' => 'theme-options',
        'capability' => 'edit_theme_options',
        'position' => '999',
        'autoload' => true,
        'icon_url'      => 'dashicons-edit-large'
    ]);

    acf_add_options_sub_page([
        'page_title' 	=> 'The options settings',
        'menu_title'	=> 'Opciones de la home',
        'parent_slug'	=> 'theme-options',
        'capability' => 'edit_theme_options',
    ]);

    acf_add_options_sub_page([
        'page_title' 	=> 'Opciones para entradas',
        'menu_title'	=> 'Entradas',
        'parent_slug'	=> 'theme-options',
    ]);

    acf_add_options_sub_page([
        'page_title' 	=> 'Opciones generales',
        'menu_title'	=> 'Generales',
        'parent_slug'	=> 'theme-options',
    ]);




$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'acf-options-opciones-de-la-home');
$options
    ->addTab('Header de la home', ['placement' => 'left'])
        ->addTrueFalse('verVideoHeader', [
            'label' => 'Activar / Desactivar Video en seccion superior',
            'instructions' => 'Activar o desactivar la opción de la seccion superior',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'Activado',
            'ui_off_text' => 'Desactivado',
        ])
        ->addText('videoUrl', [
            'label' => 'Url para el video',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '1',
            ],
        ])
        ->addImage('imagenIzquierda', [
            'label' => 'Imagen de la parte izquierda del slide',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '0',
            ],
        ])
        ->addImage('imagenDerecha', [
            'label' => 'Imagen de la parte derecha del slider',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '0',
            ],
        ])
        ->addText('txtBotonHeader', [
            'label' => 'Texto para el boton de comprar entradas de la parte superior',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '0',
            ],
        ])
        ->addUrl('linkBotonHeader', [
            'label' => 'Enlace para venta entradasr',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '0',
            ],
        ])
        ->addTextarea('iframeVideo', [
        'label' => 'Iframe para el video de la home',
            'conditional_logic' => [
                'field' => 'verVideoHeader',
                'operator' => '==',
                'value' => '0',
            ],
    ])
    ->addTab('Apartado Opiniones y Premios', ['placement' => 'left'])
        ->addGroup('seccionOpiniones', [
            'label' => 'Seccion Opiniones y premios de la home',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccioneshome'))
        ->endGroup()
        ->addRepeater('premiosListado', [
            'label' => 'Listado de premios',
            'instructions' => 'Introducir los premios del musical',
            'min' => 0,
            'max' => 4,
            'layout' => 'table',
            'button_label' => 'Agregar Premio',
            'sub_fields' => [],
        ])
            ->addText('nomPremio', [
                'label' => 'Nombre del premio',
            ])
            ->addText('exPremioPremio', [
            'label' => 'Texto acompaña el premio',
        ])
        ->endRepeater()
        ->addImage('imgPremios', [
            'label' => 'Imagen fondo premios',
        ])
        ->addText('textInferiorPremios', [
            'label' => 'Texto inferior',
        ])
        ->addTextarea('svgLetras', [
            'label' => 'svgLetras',
        ])
        ->addImage('imagenLetras', [
        'label' => 'Imagen de la parte la magia',
    ])
    ->addTab('Apartado Sinopsis', ['placement' => 'left'])
        ->addGroup('secApartadoSinopsis', [
            'label' => 'Seccion apartado sinopsis',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccioneshome'))
            ->addText('tituloSinopsis', [
                'label' => 'Titulo para la Sinopsis',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addImage('imagenSinopsis', [
                'label' => 'Imagen de la parte de sinopsis',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addWysiwyg('txtSinopsis', [
                'label' => 'texto para el paratdo de sinopsis Izquierda',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addImage('fondoSupSinopsis', [
                    'label' => 'Fondo Parte superior',
                    'conditional_logic' => [
                        'field' => 'activeSeccion',
                        'operator' => '==',
                        'value' => '1',
                    ],
                ])
            ->addImage('fondoInfSinopsis', [
                    'label' => 'Fondo Parte Inferior',
                    'conditional_logic' => [
                        'field' => 'activeSeccion',
                        'operator' => '==',
                        'value' => '1',
                    ],
                ])
        ->endGroup()

    ->addTab('Recordatorio Entradas', ['placement' => 'left'])
        ->addGroup('seccionRecorEntradas', [
            'label' => 'Seccion para el recordatorio de entradas',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccioneshome'))
            ->addText('txtRecordatorioEntradas', [
                'label' => 'texto para el apartado recordatorio entradas texto principal',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addText('txtRecordatorioDos', [
                'label' => 'texto para el apartado recordatorio entradas segundo texto',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addText('txtBotonEntradas', [
                'label' => 'Texto para el botón de comprar entradas',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addText('txtEnlaceBoton', [
                'label' => 'Texto para el enlace del boton',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
        ->endGroup()
            ->addImage('imgEntradas', [
                'label' => 'Imagen para el recordatorio de entradas',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
    ->addTab('Equipo Creativo', ['placement' => 'left'])
        ->addGroup('seccionEquipoCreativo', [
            'label' => 'Seccion para el recordatorio de entradas',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'layout' => 'Row',
            'sub_fields' => [],
        ])
            ->addFields(get_field_partial('partials.estiloseccioneshome'))
            ->addText('titEquipoCreativo', [
            'label' => 'Titulo para seccion Equipo Creativo',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],

        ])
            ->addRepeater('EquipoCreativo', [
                'label' => 'Equipo creativo',
                'conditional_logic' => [
                    'field' => 'activeSeccion',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
                ->addText('puestoEquipo', [
                    'label' => 'Puesto en el equipo creativo',
                ])
                ->addText('nombreEquipo', [
                    'label' => 'Nombre de la o las personas',
                ])
            ->endRepeater()
        ->endGroup()
    ->addTab('Te ayudamos', ['placement' => 'left'])
        ->addText('titAyudamos', [
        'label' => 'Titulo para Te ayudamos',
        'conditional_logic' => [
            'field' => 'activeSeccion',
            'operator' => '==',
            'value' => '1',
        ],

    ])
        ->addWysiwyg('txtAyudamos', [
        'label' => 'texto para te ayudamos',
        'conditional_logic' => [
            'field' => 'activeSeccion',
            'operator' => '==',
            'value' => '1',
        ],
    ])
        ->addTextarea('svgSalmandra', [
        'label' => 'svg para te ayudamos',
    ])
    ->addTab('Destacado especiales', ['placement' => 'left'])
            ->addTrueFalse('mostrarDestacado', [
                'label' => 'Mostra seccion especial',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => 'Si',
                'ui_off_text' => 'No',
            ])
            ->addText('titDestacadoEspecial', [
                'label' => 'Titulo Para el destacado especial',
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addText('subtitDestacadoEspecial', [
                'label' => 'Subtitulo para el destacado especial',
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addText('preDestacadoEspecial', [
                'label' => 'Texto Sobre imagen',
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])

            ->addImage('imagenDestacadoEspecial', [
                'label' => 'Imagen para el destacado especial',
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
            ->addImage('imagenEspecialDerecha', [
                    'label' => 'Imagen para la parte derecha especial',
                    'conditional_logic' => [
                        'field' => 'mostrarDestacado',
                        'operator' => '==',
                        'value' => '1',
                    ],
                ])
            ->addUrl('urlEspecial', [
                'label' => 'URL especial para la compra tarjeta',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],

            ])
            ->addRepeater('listadoEspeciales', [
                'label' => 'Items para listado especiales',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    'field' => 'mostrarDestacado',
                    'operator' => '==',
                    'value' => '1',
                ],
            ])
                ->addText('itemListadoEspeciales', [
                    'label' => 'Item para el listado de los especiales',
                    'conditional_logic' => [
                        'field' => 'mostrarDestacado',
                        'operator' => '==',
                        'value' => '1',
                    ],
                ])
            ->EndRepeater()





;

return $options;
