<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;


$programaespecial = new FieldsBuilder('programaespecial');

$programaespecial
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-programaespecial.blade.php');

$programaespecial
    ->addTab('Programa especial', ['placement' => 'left'])
        ->addImage('imagenEspecial', [
            'label' => 'Imagen para pase especial',
        ])
        ->addImage('logoEspecial', [
            'label' => 'Logo para pase especial',
        ])
        ->addText('textoEspecial', [
            'label' => 'Texto Intro para landing especial',
        ])

;
return $programaespecial;

