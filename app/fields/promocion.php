<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$promocion = new FieldsBuilder('campos_promocion');

$promocion
    ->setLocation('post_type', '==', 'promocion');

$promocion
    ->addImage('bannerPromocion', [
        'label' => 'Banner para la promocion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addUrl('enlacePromocion', [
        'label' => 'Enlace para la promocion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])


;
return $promocion;

