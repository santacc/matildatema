<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
   // add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer Left', 'sage'),
        'id'            => 'sidebar-footer-left'
    ] + $config);
    register_sidebar([
            'name'          => __('Footer Right', 'sage'),
            'id'            => 'sidebar-footer-right'
        ] + $config);
    register_sidebar([
            'name'          => __('Footer Bottom', 'sage'),
            'id'            => 'sidebar-footer-bottom'
        ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Initialize ACF Builder
 */
add_action('init', function () {
    collect(glob(config('theme.dir').'/app/fields/*.php'))->map(function ($field) {
        return require_once($field);
    })->map(function ($field) {
        if ($field instanceof FieldsBuilder) {
            acf_add_local_field_group($field->build());
        }
    });

    register_extended_post_type( 'opinion', [
        'labels' => [
            "menu_name" => __( "Opiniones", "sage" ),
            "all_items" => __( "Todas las opiniones", "sage" ),
            "add_new" => __( "Añadir nuevo", "sage" ),
            "add_new_item" => __( "Añadir nueva opinión", "sage" ),
            "edit_item" => __( "Editar opinión", "sage" ),
            "new_item" => __( "Nueva opinión", "sage" ),
        ],


        'supports' => ["title","thumbnail"],
        'edit' => false,
        'block_editor' => true,
        # Add the post type to the site's main RSS feed:
        'show_in_feed' => true,

        # Show all posts on the post type archive:
        'archive' => [
            'nopaging' => true,
        ],

        # Add the post type to the 'Recently Published' section of the dashboard:
        'dashboard_activity' => true,

        # Add some custom columns to the admin screen:
        'admin_cols' => [
            'story_featured_image' => [
                'title'          => 'Imagen destacada',
                'featured_image' => 'thumbnail'
            ],
            'story_published' => [
                'title_icon'  => 'dashicons-calendar-alt',
                'meta_key'    => 'published_date',
                'date_format' => 'd/m/Y'
            ],
        ],

        # Add some dropdown filters to the admin screen:
        'admin_filters' => [
            'story_rating' => [
                'meta_key' => 'star_rating',
            ],
        ],

    ], array(

        # Override the base names used for labels:
        'singular' => 'Opinion',
        'plural'   => 'Opiniones',
        'slug'     => 'opinion',

    ) );

    register_extended_post_type( 'actor', [
        'labels' => [
            "menu_name" => __( "Elenco", "sage" ),
            "all_items" => __( "Todos los actores", "sage" ),
            "add_new" => __( "Añadir nuevo actor", "sage" ),
            "add_new_item" => __( "Añadir nuevo actor", "sage" ),
            "edit_item" => __( "Editar Actor", "sage" ),
            "new_item" => __( "Nuevo actor", "sage" ),
        ],


        'supports' => ["title","thumbnail"],
        'edit' => false,
        'block_editor' => true,
        # Add the post type to the site's main RSS feed:
        'show_in_feed' => true,

        # Show all posts on the post type archive:
        'archive' => [
            'nopaging' => true,
        ],

        # Add the post type to the 'Recently Published' section of the dashboard:
        'dashboard_activity' => true,

        # Add some custom columns to the admin screen:
        'admin_cols' => [
            'my_column' => array(
                'title_icon'  => 'dashicons-admin-users',
                'title'       => 'Personaje principal',
                'meta_key'    => 'personajePrincipal',
            ),
            'my_column2' => array(
                'title'       => 'Foto',
                'meta_key'    => 'fotoActor',
            ),
        ],

        # Add some dropdown filters to the admin screen:
        'admin_filters' => [
            'story_rating' => [
                'meta_key' => 'star_rating',
            ],
        ],

    ], array(

        # Override the base names used for labels:
        'singular' => 'Actor',
        'plural'   => 'Actores',
        'slug'     => 'actor',

    ) );

    register_extended_post_type( 'promocion', [
        'labels' => [
            "menu_name" => __( "Promociones", "sage" ),
            "all_items" => __( "Todas las promociones", "sage" ),
            "add_new" => __( "Añadir nuevo", "sage" ),
            "add_new_item" => __( "Añadir nueva promoción", "sage" ),
            "edit_item" => __( "Editar promoción", "sage" ),
            "new_item" => __( "Nueva promocion", "sage" ),
        ],


        'supports' => ["title","thumbnail"],
        'edit' => false,
        'block_editor' => true,
        # Add the post type to the site's main RSS feed:
        'show_in_feed' => true,

        # Show all posts on the post type archive:
        'archive' => [
            'nopaging' => true,
        ],

        # Add the post type to the 'Recently Published' section of the dashboard:
        'dashboard_activity' => true,

        # Add some custom columns to the admin screen:
        'admin_cols' => [
            'story_featured_image' => [
                'title'          => 'Imagen destacada',
                'featured_image' => 'thumbnail'
            ],
            'story_published' => [
                'title_icon'  => 'dashicons-calendar-alt',
                'meta_key'    => 'published_date',
                'date_format' => 'd/m/Y'
            ],
        ],

        # Add some dropdown filters to the admin screen:
        'admin_filters' => [
            'story_rating' => [
                'meta_key' => 'star_rating',
            ],
        ],

    ], array(

        # Override the base names used for labels:
        'singular' => 'Promocion',
        'plural'   => 'Promociones',
        'slug'     => 'promocion',

    ) );

});



